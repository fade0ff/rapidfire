/** DMA library
	Use of Direct Memory Access controller
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2017 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#include "global.h"
#include "prc.h"
#include "int_cnf.h"
#include "dma.h"

dma_callback_t dma_callbacks[DMA_NUM_CHANNELS]; ///! Array of callback pointers for functions to be called at end of transfer

/** DMA entry table - must be 512 byte aligned */
#if defined(__CC_ARM) /* Keil alignment to 512 bytes */
__align(512) dma_entry_t dma_entry_table[DMA_NUM_CHANNELS];
#endif /* defined (__CC_ARM) */

#if defined(__ICCARM__) /* IAR EWARM alignment to 512 bytes */
#pragma data_alignment=512
dma_entry_t dma_entry_table[DMA_NUM_CHANNELS];
#endif /* defined (__ICCARM__) */

#if defined( __GNUC__ ) /* GNU alignment to 512 bytes */
dma_entry_t dma_entry_table[DMA_NUM_CHANNELS] __attribute__ ((aligned(512)));
#endif /* defined (__GNUC__) */

/** Define callback function for end of transfer.
	@param ch channel identifier
	@param callback function pointer for callback function
*/
void DMA_SetCallBack(u8 ch, dma_callback_t callback) {
	dma_callbacks[ch] = callback;
}

/** Enable/disable common DMA interrupt.
	@param en 1: enable, 0: disable
*/
void DMA_IRQEnable(u8 en) {
	if (en != 0) {
		NVIC_SetPriority(DMA_IRQn, DMA_IRQ_PRIO);
		NVIC_EnableIRQ(DMA_IRQn);
	} else
		NVIC_DisableIRQ(DMA_IRQn);
}

/** Global interrupt for DMA channels. */
void DMA_IRQHandler(void) {
	u32 pendinga = DMA_GetIntA();
	u32 pendingb = DMA_GetIntB();
	u32 err = DMA_GetErrInt();
	u32 tmp,idx;
	tmp = pendinga | pendingb;           // handle A and B the same

	/* loop though all set bits, beginning with the most significant set bit */
	do {
		idx = PRC_GetMSB(tmp);
		if (idx > 0) {
			tmp &= (u32)~(1UL<<(idx-1)); // remove bit from bitmask
			if (dma_callbacks[idx-1] != NULL)
				(*dma_callbacks[idx-1])();
		} else
			break;                       // no more bits set in bitmask -> leave loop
	} while(1);

	DMA_ClrErrInt(err);
	DMA_ClrIntA(pendinga);
	DMA_ClrIntB(pendingb);
}

/** DMA initialization function.
	Call first before using any other functions of this library.
*/
void DMA_Init(void) {
	DMA_SetBaseAdr(dma_entry_table);
	/* setup DMA */
	DMA_ClrIntEnable((1UL<<DMA_NUM_CHANNELS)-1);
	DMA_ClrErrInt((1UL<<DMA_NUM_CHANNELS)-1);                   /* clear error interrupt requests for all channel */
	DMA_ClrIntA((1UL<<DMA_NUM_CHANNELS)-1);                     /* clear interrupt A requests for all channels */
	DMA_ClrIntB((1UL<<DMA_NUM_CHANNELS)-1);                     /* clear interrupt B requests for all channels */
	DMA_SetCtrl(DMA_CTRL_ENABLE);
}

