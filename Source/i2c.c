/** I2C library
	Communication via I2C peripheral (interrupt driven)
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#include "global.h"
#include "sys.h"
#include "i2c.h"
#include "prc.h"
#include "int_cnf.h"

/* Local macros for fault handling */
#define I2C_CHECK_OK    1 ///! All is good, continue with transfer
#define I2C_CHECK_RETRY 2 ///! Something went wrong, retry
#define I2C_CHECK_STOP  3 ///! No recovery possible, stop transfer

/** Macro for repetitive fault reactions */
#define I2C_HandleTransferFault(x) {                       \
	if (++(dev_ram_ptr->retry_ctr) > ch_cfg_ptr->retries){ \
		ch_ram_ptr->faults |= (x);                         \
		check_st = I2C_CHECK_STOP;                         \
	 } else check_st = I2C_CHECK_RETRY;	}


u8 i2c_virtual_device_id[I2C_PHYS_DEVICE_NUM];                  ///! to get virtual device number from physical I2C device number
u8 i2c_device_irq[I2C_VIRTUAL_DEVICE_NUM];                      ///! to get IRQ number from
i2c_device_t i2c_device_ram[I2C_VIRTUAL_DEVICE_NUM];            ///! array of RAM structures for all configured devices
i2c_queue_t i2c_queues[I2C_VIRTUAL_DEVICE_NUM][I2C_QUEUE_SIZE]; ///! I2C queues
i2c_channel_t i2c_channel_ram[I2C_CHANNEL_CONFIG_NUM];          ///! array of RAM structures for all configured channels

/** Get virtual device ID for channel
	@param ch channel identifier
	@return virtual device ID (configuration order, not physical order)
 */
u8 I2C_GetDeviceID(u8 ch_id) {
	const i2c_channel_cfg_t *ch_cfg_ptr = &i2c_channel_cfg[ch_id];
	return ch_cfg_ptr->device_id;
}

/** Get physical device for channel
	@param ch channel identifier
	@return physical device (pointer to I2C peripheral)
 */
LPC_I2C_Type* I2C_GetDevice(u8 ch_id) {
	const i2c_channel_cfg_t *ch_cfg_ptr = &i2c_channel_cfg[ch_id];
	return (LPC_I2C_Type *)i2c_device_cfg[ch_cfg_ptr->device_id];
}

/** Set data rate and duty cycle
	@param frq      desired frequency
	@param dc       duty cycle of high level (0x8000 == 100%)
	@param scl_low  low part of clock divider
	@param scl_high high part of clock divider
 */
void I2C_CalcTiming(u32 frq, u16 dc, u8 *scl_low, u8 *scl_high, u16 *fdiv) {
	u32 div, devfreq, val;

	// calculate sensible divider (low + high clocks can be 18 at maximum, leave a bit room for dc)
	div = SYS_CLK_I2C_FRQ/(10*frq);
	*fdiv = div;
	devfreq =  SYS_CLK_I2C_FRQ / div;

	// calculate desired divider
	div = (devfreq + frq/2)/frq;
	// note: the low and high clocks can only be configured from 2 to 9 (0..7 internally)
	val = ( (div * dc) + (I2C_DC_SCALE/2))/I2C_DC_SCALE;
	*scl_high = val-2;
	*scl_low = (div - val)-3; // manual seems to be wrong!!!!
}

/** Initialize I2C driver - call this before using any other driver function */
void I2C_Init(void) {
	int ch_id;
	for (ch_id=0; ch_id<I2C_CHANNEL_CONFIG_NUM; ch_id++) {
		u8 tprof_idx = i2c_channel_cfg[ch_id].timing_profile_idx;
		u32 frq = i2c_timing_profile_cfg[tprof_idx].baudrate;
		u16 dc  = i2c_timing_profile_cfg[tprof_idx].duty_cycle;
		I2C_CalcTiming(frq, dc, &i2c_channel_ram[ch_id].scll, &i2c_channel_ram[ch_id].sclh, &i2c_channel_ram[ch_id].divider);
		i2c_channel_ram[ch_id].pending_count = 0;
		i2c_channel_ram[ch_id].faults = 0;
	}

	int device_id;
	u8 irq;
	for (device_id=0; device_id<I2C_VIRTUAL_DEVICE_NUM; device_id++) {
		LPC_I2C_TypeDef *pdevice = (LPC_I2C_TypeDef *)i2c_device_cfg[device_id];
		u8 phys_device_id;

		switch ((u32)pdevice) {
			case (u32)LPC_I2C0:
				irq = I2C0_IRQn;
				phys_device_id = 0;
				break;
			case (u32)LPC_I2C1:
				irq = I2C1_IRQn;
				phys_device_id = 1;
				break;
			case (u32)LPC_I2C2:
				irq = I2C2_IRQn;
				phys_device_id = 2;
				break;
			default:
				irq = I2C3_IRQn;
				phys_device_id = 3;
		}
		i2c_virtual_device_id[phys_device_id] = device_id;
		i2c_device_irq[device_id] = irq;
		NVIC_SetPriority(irq, I2C_IRQ_PRIO);
		NVIC_EnableIRQ(irq);
	}
}

static void I2C_StartTransfer(LPC_I2C_Type *pdevice, i2c_device_t *dev_ram_ptr, const i2c_channel_cfg_t *ch_cfg_ptr, u8 slave_address, u8 type) {
	u8 rw;
	I2C_SetConfig(pdevice,I2C_CFG_MSTEN);
	while( (I2C_GetStatus(pdevice) & I2C_STAT_MSTPENDING) == 0);
	if ((I2C_GetStatus(pdevice) & I2C_STAT_MSTSTATE_MASK) != I2C_STAT_MSTSTATE_IDLE)
		return;
	if ((type == I2C_SLAVE_ADR_M_TX_READ) && (ch_cfg_ptr->adr_size == I2C_ADR_SIZE_NONE)) {
		// more a hypothetical case: read access without an address to write to the slave
		rw = I2C_SLAVE_ADR_M_TX_READ;
		dev_ram_ptr->state = I2C_STATE_DATA_READ;
		dev_ram_ptr->transfer_count = 0;
	} else {
		rw = I2C_SLAVE_ADR_M_TX_WRITE;
		if (ch_cfg_ptr->adr_size != I2C_ADR_SIZE_NONE) {
			// normal case: always start with a write for the address
			dev_ram_ptr->state = I2C_STATE_ADR_WRITE;
			/* misuse transfer count as shift counter for address */
			dev_ram_ptr->transfer_count = 8*(ch_cfg_ptr->adr_size);
		} else {
			// exotic case (e.g. TM1637): start writing directly
			dev_ram_ptr->state = I2C_STATE_DATA_WRITE;
			dev_ram_ptr->transfer_count = 0;
		}
	}
	I2C_SetData(pdevice, (slave_address<<1) | rw);
	I2C_SetMasterControl(pdevice, I2C_MSTCTL_MSTSTART);
}

/**
  Initiate Transfer with explicit slave address
  @param ch_id Reference to configuration channel
  @param slave_adress slave address to use instead of configuration value
  @param type Type of transfer
  @param address 8..32bit address
  @param buffer_ptr pointer to source/target buffer
  @param len size of buffer in bytes
  @param i2c_callback_t callback function (NULL if none needed)
 */
u8 I2C_MasterTransferX(u8 ch_id, u8 slave_address, u8 type, u32 address, u8* buffer_ptr, u16 len, i2c_callback_t callback) {
	const i2c_channel_cfg_t *ch_cfg_ptr = &i2c_channel_cfg[ch_id];
	int device_id = ch_cfg_ptr->device_id;
	i2c_device_t *dev_ram_ptr = &i2c_device_ram[device_id];
	LPC_I2C_Type *pdevice = (LPC_I2C_Type *)i2c_device_cfg[device_id];
	i2c_channel_t *ch_ram_ptr = &i2c_channel_ram[ch_id];
	i2c_queue_t *q;
	u8 retval;

	PRC_Int_Disable();
	{
		if (dev_ram_ptr->state == I2C_STATE_INACTIVE && dev_ram_ptr->queue_entries==0) {
			dev_ram_ptr->active_ch = ch_id;
			I2C_SetDivider(pdevice, ch_ram_ptr->divider);
			I2C_SetMasterTime(pdevice, ch_ram_ptr->scll, ch_ram_ptr->sclh);
			I2C_SetIntEnable(pdevice,I2C_INTENSET_MSTPENDING);
			I2C_StartTransfer(pdevice, dev_ram_ptr, ch_cfg_ptr, slave_address, type);
			retval = I2C_MST_TRANSFER_OK;
		} else {
			// put channel in queue
			if (dev_ram_ptr->queue_entries < I2C_QUEUE_SIZE)
				retval = I2C_MST_TRANSFER_QUEUED;
			else {
				retval = I2C_MST_TRANSFER_FAILED;
				dev_ram_ptr->overflow = 1;
			}
		}
		/* Note: even a directly started transfer is using one queue entry to simplify parameter storage
	       Therefore the queue_bottom_idx points to the current transfer (if there is one) and
	       queue_entries is 1 even if there is actually no transfer queued.
		 */
		if (retval != I2C_MST_TRANSFER_FAILED) {
			q = &i2c_queues[device_id][(dev_ram_ptr->queue_bottom_idx+dev_ram_ptr->queue_entries)%I2C_QUEUE_SIZE];
			q->address = address;
			q->buffer_ptr = buffer_ptr;
			q->ch_id = ch_id;
			q->len = len;
			q->callback = callback;
			q->type = type;
			q->slave_address = slave_address;
			dev_ram_ptr->queue_entries++;
			ch_ram_ptr->pending_count++;
		}
	}
	PRC_Int_Enable();
	return retval;
}

/**
  Initiate Transfer
  @param ch_id Reference to configuration channel
  @param type Type of transfer
  @param address 8..32bit address
  @param buffer_ptr pointer to source/target buffer
  @param len size of buffer in bytes
  @param i2c_callback_t callback function (NULL if none needed)
 */
u8 I2C_MasterTransfer(u8 ch_id, u8 type, u32 address, u8* buffer_ptr, u16 len, i2c_callback_t callback) {
	u8 slave_address = i2c_channel_cfg[ch_id].slave_address;
	return I2C_MasterTransferX(ch_id, slave_address, type, address, buffer_ptr, len, callback);
}

/** Check if queue entries are available
	@param ch_id channel identifier
	@return number of queue entries available for the channel's device
 */
u8 I2C_QueueEntriesAvailable(u8 ch_id) {
	u8 device_id = i2c_channel_cfg[ch_id].device_id;
	i2c_device_t *dev_ram_ptr = &i2c_device_ram[device_id];
	return I2C_QUEUE_SIZE - dev_ram_ptr->queue_entries;
}

/** Return number of pending transfers for a given channel
	@param ch_id channel identifier
	@return number of pending queue entries for this channel
 */
u8 I2C_GetChannelPendingCount(u8 ch_id) {
	return i2c_channel_ram[ch_id].pending_count;
}

/** Read and reset faults for a given channel
	@param ch_id channel identifier
	@return bitmask containing states [I2C_STATE_xxx] where a fault occured
 */
u16 I2C_GetFaults(u8 ch_id) {
	u16 faults;
	i2c_channel_t *ch_ram_ptr = &i2c_channel_ram[ch_id];

	PRC_Int_Disable();
	{
		faults = ch_ram_ptr->faults;
		ch_ram_ptr->faults = 0;
	}
	PRC_Int_Enable();
	return faults;
}

/** Interrupt handler
	@param device_id virtual device identifier (wrapping done by physical device handlers)
 */
void I2C_IRQHandler(u8 device_id) {
	LPC_I2C_Type *pdevice = (LPC_I2C_Type *)i2c_device_cfg[device_id];
	i2c_device_t *dev_ram_ptr = &i2c_device_ram[device_id];
	u8 ch_id = dev_ram_ptr->active_ch;
	i2c_channel_t *ch_ram_ptr = &i2c_channel_ram[ch_id];
	i2c_queue_t *queue_ch_ptr = &i2c_queues[device_id][dev_ram_ptr->queue_bottom_idx];
	const i2c_channel_cfg_t *ch_cfg_ptr = &i2c_channel_cfg[ch_id];
	u32 status = I2C_GetStatus(pdevice); // status read from device register I2CSTAT
	u8 callback = 0;
	u8 check_st = I2C_CHECK_OK;

	if ( (status & I2C_STAT_MSTRARBLOSS) != 0) {
		I2C_HandleTransferFault(I2C_STATE_ARB_LOST);
		I2C_ClrStatus(pdevice, I2C_STAT_MSTRARBLOSS);
	} else if ( (status & I2C_STAT_MSTSTSTPERR) != 0) {
		I2C_HandleTransferFault(I2C_STATE_START_STOP_ERR);
		I2C_ClrStatus(pdevice, I2C_STAT_MSTSTSTPERR);
	} else if ( (status & I2C_STAT_MSTPENDING) != 0) {
		status &= I2C_STAT_MSTSTATE_MASK;
		if (check_st == I2C_CHECK_OK) {
			switch (dev_ram_ptr->state) {
				case I2C_STATE_DATA_READ:
					if (status == I2C_STAT_MSTSTATE_RXREADY) {
						if (queue_ch_ptr->len != 0)
							queue_ch_ptr->buffer_ptr[dev_ram_ptr->transfer_count] = I2C_GetData(pdevice);
						if (dev_ram_ptr->transfer_count+1 < queue_ch_ptr->len) {
							dev_ram_ptr->state = I2C_STATE_DATA_READ;
							I2C_SetMasterControl(pdevice, I2C_MSTCTL_MSTCONTINUE);
						} else {
							check_st = I2C_CHECK_STOP;
							callback = 1;
						}
						dev_ram_ptr->transfer_count++;
					} else {
						I2C_HandleTransferFault(dev_ram_ptr->state);
					}
					break;
				case I2C_STATE_ADR_WRITE: // write address to slave
					if (status == I2C_STAT_MSTSTATE_TXREADY) {
						I2C_SetData(pdevice, (u8)queue_ch_ptr->address);
						I2C_SetMasterControl(pdevice, I2C_MSTCTL_MSTCONTINUE);
						dev_ram_ptr->transfer_count-=8;
						if (dev_ram_ptr->transfer_count == 0) {
							if (queue_ch_ptr->len != 0) {
								if (queue_ch_ptr->type == I2C_TYPE_MST_READ)
									dev_ram_ptr->state = I2C_STATE_REPEAT_START;
								else
									dev_ram_ptr->state = I2C_STATE_DATA_WRITE;
							} else{
								check_st = I2C_CHECK_STOP;
								callback = 1;
							}
						}
					} else {
						I2C_HandleTransferFault(dev_ram_ptr->state);
					}
					break;
				case I2C_STATE_REPEAT_START:
					I2C_SetData(pdevice, (queue_ch_ptr->slave_address<<1) | I2C_SLAVE_ADR_M_TX_READ);
					I2C_SetMasterControl(pdevice, I2C_MSTCTL_MSTSTART);
					dev_ram_ptr->state = I2C_STATE_DATA_READ;
					break;
				case I2C_STATE_DATA_WRITE:
					if (status == I2C_STAT_MSTSTATE_TXREADY) {
						if (dev_ram_ptr->transfer_count < queue_ch_ptr->len) {
							I2C_SetData(pdevice, queue_ch_ptr->buffer_ptr[dev_ram_ptr->transfer_count]);
							I2C_SetMasterControl(pdevice, I2C_MSTCTL_MSTCONTINUE);
							dev_ram_ptr->transfer_count++;
						} else {
							check_st = I2C_CHECK_STOP;
							callback = 1;
						}
					} else {
						I2C_HandleTransferFault(dev_ram_ptr->state);
					}
			}
		}
	}

	if (check_st != I2C_CHECK_OK) {
		if (check_st == I2C_CHECK_RETRY) {
			I2C_StartTransfer(pdevice, dev_ram_ptr, ch_cfg_ptr, queue_ch_ptr->slave_address, queue_ch_ptr->type);
		} else {
			// transmission finished or failed
			while( (I2C_GetStatus(pdevice) & I2C_STAT_MSTPENDING) == 0);
			I2C_SetMasterControl(pdevice, I2C_MSTCTL_MSTSTOP);
			// schedule >
			if (dev_ram_ptr->queue_entries > 0)
				dev_ram_ptr->queue_entries--;
			if (dev_ram_ptr->queue_entries > 0) {
				/* start next transmission */
				dev_ram_ptr->queue_bottom_idx = (dev_ram_ptr->queue_bottom_idx + 1)%I2C_QUEUE_SIZE;
				ch_id = i2c_queues[device_id][dev_ram_ptr->queue_bottom_idx].ch_id;
		        dev_ram_ptr->active_ch = ch_id;
		        dev_ram_ptr->retry_ctr = 0;
		        I2C_SetDivider(pdevice, i2c_channel_ram[ch_id].divider);
		        I2C_SetMasterTime(pdevice, i2c_channel_ram[ch_id].scll, i2c_channel_ram[ch_id].sclh); // ch_id changed!
		        I2C_StartTransfer(pdevice, dev_ram_ptr, &i2c_channel_cfg[ch_id], i2c_queues[device_id][dev_ram_ptr->queue_bottom_idx].slave_address, i2c_queues[device_id][dev_ram_ptr->queue_bottom_idx].type);
			} else {
				dev_ram_ptr->state = I2C_STATE_INACTIVE;
				I2C_ClrIntEnable(pdevice,I2C_INTENSET_MSTPENDING);
			}
			// <schedule
			ch_ram_ptr->pending_count--;                           // still points to old channel
			if ((callback!=0) && (queue_ch_ptr->callback != NULL)) // still points to old queue position
				(*queue_ch_ptr->callback)();
		}
	}
}

/** Interrupt handler for physical device 0 -> wrapping to virtual device */
void I2C0_IRQHandler(void) {
	I2C_IRQHandler(i2c_virtual_device_id[0]);
}

/** Interrupt handler for physical device 1 -> wrapping to virtual device */
void I2C1_IRQHandler(void) {
	I2C_IRQHandler(i2c_virtual_device_id[1]);
}

/** Interrupt handler for physical device 2 -> wrapping to virtual device */
void I2C2_IRQHandler(void) {
	I2C_IRQHandler(i2c_virtual_device_id[2]);
}

/** Interrupt handler for physical device 2 -> wrapping to virtual device */
void I2C3_IRQHandler(void) {
	I2C_IRQHandler(i2c_virtual_device_id[3]);
}
