/** NVM library
	Non volatile memory simulation implemented via flash
	LPC8xx ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2017 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef NVM_H
#define NVM_H

#include "nvm_cnf.h"

/* Prototypes */
extern void NVM_Init(void);
extern u8 NVM_Write(u8 adr, u8 *data, u8 len);
extern u8 NVM_Read(u8 adr, u8 *data, u8 len);
extern void NVM_WaitForCompletion(void);

#endif
