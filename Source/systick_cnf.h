/** System Tick library - Configuration
	Access to SysTick timer
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef SYSTICK_CNF_H
#define SYSTICK_CNF_H

#define SYSTICK_PERIOD_US 1000 ///! SysTick Period in microseconds

#endif
