/** System Tick library
	Access to SysTick timer
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#include "global.h"
#include "sys.h"
#include "systick.h"
#include "systick_cnf.h"

/** Calculate the SysTick timer reload value from the configured period */
#define SYSTICK_RELOAD_VAL (((((u64)SYS_FRQ_CORE*SYSTICK_PERIOD_US+500000)/1000000)-1))

/** Note! The Reload value is limited to 24bit.
   For e.g. for 100MHZ, the minimum frequency is 5.96Hz
   which equals a maximum period of 167.78ms.
   For some reason GCC doesn't allow casts in expression used in precompiler
   conditionals. Therefore SYSTICK_RELOAD_VAL can't be used here...
 */
#if ((((SYS_FRQ_CORE*SYSTICK_PERIOD_US+500000)/1000000)-1)) > 0xffffff
#error "The Systick reload value exceeds the maximum possible value!"
#endif


/** Initialize the SysTick timer */
void SYSTICK_Init(void) {
	SysTick->CTRL |= SYSTICK_CTRL_CLKSOURCE;
	SysTick->LOAD = SYSTICK_RELOAD_VAL;
}

