#ifndef WKT_H
#define WKT_H

/** WKT library
	Self-wake-up Timer
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2017 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

/* WKT control register */

#define WKT_CTRL_CLKSEL_750KHZ  0         ///! Use divided IRC clock (750kHz). Not available in deep sleep or power down
#define WKT_CTRL_CLKSEL_10KHZ   (1UL<<0)  ///! Use low power clock (10kHz). Only +/-40% accuracy. Must be enabled separately
#define WKT_CTRL_ALARMFLAG      (1UL<<1)  ///! Set when time out occured. Write 1 to clear
#define WKT_CTRL_CLEARCTR       (1UL<<2)  ///! Clear the self-wake-up timer by writing a 1
#define WKT_CTRL_SEL_EXTCLK     (1UL<<3)  ///! Select in external clock for the self-wake-up timer

/** Get WKT control register.
	@return CTRL register
*/
#define WKT_GetIntFlags() (LPC_WKT->CTRL)

/** Clear bits in WKT control register.
	@param y bitmask with bits to clear
*/
#define WKT_ClrIntFlags(y)  {LPC_WKT->CTRL = (u32)(y);}
	
	
/* WKT count register */	

/** Get WKT count register.
	@return COUNT register
*/
#define WKT_GetCount() (LPC_WKT->COUNT)

/** Set WKT count register.
	@param y value to write to COUNT register
*/
#define WKT_SetCount(y)  {LPC_WKT->COUNT = (u32)(y);}

#endif