/** RapidFire
	A simple rapid fire implementation as drop in for Competition Pro USB reworked to a classic C64/Amiga joystick
	2nd Button functions suppports C64 or Amiga.
	Press IN1B during power up to enter programming mode.
	Pressing IN2B at the same time switches to C64 mode, else Amiga mode will be used.
	This setting is stored in NVRAM and used until programming mode is entered again.
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2020 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#include "global.h"
#include "sys.h"
#include "prc.h"
#include "pin.h"
#include "priowrap.h"
#include "dma.h"
#include "crc.h"
#include "nvm.h"
#include "systick.h"
#include "systime.h"
#include "mrt.h"
#include "system_LPC8xx.h"
#include <string.h>
#include <stdlib.h>

#define TRIGGER_PERIOD_DEF    2  /* toggle trigger every 20ms -> 40ms period -> 25Hz) */
#define INIT_SETUP_SAMPLES   10  /* Number of samples to take for setup check */
#define INIT_SETUP_THRESHOLD  8  /* Number of samples needed to see a button press in setup mode */
#define INIT_SETUP_WAIT_MS   10  /* Milliseconds to wait per look in the setup loop */

typedef struct {
	u8 c64_mode;
	u8 trigger_period;
} config_t;

// variables
u32 ms_counter;
u8  rapidfire_active;
u8  out1, out2;
u8  in1, in2;
u8  trigger;
u8  trigger_cnt;

config_t config = {
	.c64_mode = 0,                     ///! Button2 mode: 0: Amiga mode, 1: C64 mode (activate pullup)
	.trigger_period = TRIGGER_PERIOD_DEF,  ///! Trigger toggle period in 10ms ticks
};


u16 calc_crc(u8 *data, u16 len) {
	int i;
	CRC_SetMode(CRC_MODE_POLY_CCITT);
	CRC_SetSeed(0xFFFF);
	for (i=0; i<len; i++)
		CRC_SetData8(data[i]);
	return CRC_GetSum();
}

void task_1ms(u32 dummy) {
	(void)dummy;

	// the input buttons are low active (0 if pressed)
	in1 = !(PIN_ChGet(PIN_IN1A)&PIN_ChGet(PIN_IN1B));
	in2 = !(PIN_ChGet(PIN_IN2A)&PIN_ChGet(PIN_IN2B));

	if (rapidfire_active != 0) {
		out1 = in1 & trigger;
		out2 = in2; /* & trigger; */ // no autofire for 2nd button
	} else {
		out1 = in1;
		out2 = in2;
	}

	PIN_ChSet(PIN_OUT1, !out1);       // low active
	if (config.c64_mode) {
		PIN_ChSet(PIN_PULLUP, !out2); // low active - switch PFET on in C64 mode to activate pullup
	} else {
		PIN_ChSet(PIN_OUT2, !out2);   // low active in Amiga mode
	}
}

/** 10ms low priority task
	@param dummy unused parameter needed for Prio_Wrap_Func_t
*/
void task_10ms(u32 dummy) {
	(void)dummy;
	if (++trigger_cnt >= config.trigger_period) {
		trigger_cnt = 0;
		trigger ^= 1;
	}
}

/** 100ms low priority task
	@param dummy unused parameter needed for Prio_Wrap_Func_t
*/
void task_100ms(u32 dummy) {
	(void)dummy;

	rapidfire_active = !PIN_ChGet(PIN_SWITCH);
	PIN_ChSet(PIN_LED1, !rapidfire_active); // low active
}

void SysTick_Handler(void) {
	ms_counter++;
	PrioWrap_Function(task_1ms, 0);
	if (ms_counter % 10 == 0)
		PrioWrap_Function(task_10ms, 0);
	if (ms_counter % 100 == 0)
		PrioWrap_Function(task_100ms, 0);
}

int main(void) {
	u8 ctr1,ctr2;
	u8 buffer[sizeof(config)+2];

	SystemInit();
	SYSTIME_Init();
	PIN_Init();
	// disable switch matrix and IO config to save power
	SYSCON_DisableClockControl(SYS_CLK_SWM|SYS_CLK_IOCON);

	NVM_Init();
	// Check if buttons are pressed to switch mode
	// Note: IN2B/PIO0_4 seems to block the program from starting
	// I'm unsure if this is due to the TRST (JTAG Reset) or WAKEUP functionality
	ctr1=0; ctr2=0;
	for (int i=0; i<INIT_SETUP_SAMPLES; i++) {
		SYSTIME_WaitMs(INIT_SETUP_WAIT_MS);
		if (PIN_ChGet(PIN_IN2A)==0) // low active
			ctr1++;
		if (PIN_ChGet(PIN_IN1B)==0) // low active
			ctr2++;
	}
	if (ctr1 > INIT_SETUP_THRESHOLD) {
		// only change mode if button 2A was pressed during reset
		// use button 1B to either set Amiga mode (not pressed) or C64 mode (pressed)
		config.c64_mode = (ctr2 > INIT_SETUP_THRESHOLD);
		// write to NVRAM
		memcpy(&buffer[0],(u8*)&config, sizeof(config));
		u16 crc = calc_crc((u8*)&config, sizeof(config));
		buffer[sizeof(config)] = (u8)(crc & 0xff);
		buffer[sizeof(config)+1] = (u8)(crc >> 8);
		(void)NVM_Write(0, &buffer[0], sizeof(config)+2);
	} else {
		if (NVM_Read(0, buffer, sizeof(config)+2) != 0) {
			// check crc
			u16 crc = calc_crc((u8*)&buffer, sizeof(config));
			u16 crc_nvm = buffer[sizeof(config)] | (buffer[sizeof(config)+1]<<8);
			if (crc == crc_nvm) {
				memcpy((u8*)&config, &buffer[0], sizeof(config));
			}
		}
	}
	PIN_ChSet(PIN_LED1,   !rapidfire_active); // low active
	PIN_ChSet(PIN_LED2,   !config.c64_mode);  // low active - switch LED2 on in C64 mode

	// Init timebase
	// SysTick and PendSV are system exceptions - they don't need to be enabled
	NVIC_SetPriority(SysTick_IRQn, SYSTICK_IRQ_PRIO);   /* set to medium prio */
	SYSTICK_Init();                                     /* 1ms system tick */
	SYSTICK_Enable(1);
	SYSTICK_EnableIRQ(1);

	while(1) {
		/* Sleep until next IRQ happens */
		__WFI();
	}
	return 0 ;
}


