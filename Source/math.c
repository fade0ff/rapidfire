/** Math library
	Collection of filters, interpolation, characteristics
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#include "global.h"
#include "math.h"


/** Moving average filter for unsigned 16bit values.
	Calculate the average over a ring buffer.
	@param flt structure flt_mov_avgu16_t
 */
u16 FLT_AvgU16_Get(flt_mov_avgu16_t flt) {
	u8 index;
	u32 sum = 0;
	for (index=0; index<flt.size; index++)
		sum += flt.data[index];
	return (u16)(sum/flt.size);
}

/** Moving average filter for signed 16bit values.
	Calculate the average over a ring buffer.
	@param flt structure flt_mov_avgs16_t
 */
s16 FLT_AvgS16_Get(flt_mov_avgs16_t flt) {
	u8 index;
	s32 sum = 0;
	for (index=0; index<flt.size; index++)
		sum += flt.data[index];
	return (s16)((sum+(flt.size/2))/flt.size);
}

/** Calculate the interpolated result from a map with <size> discrete sample points.
	The size of the arrays <axis> and <value> have to be equal to <size>.
	The value inside <axis> have to be ordered from lower to higher (strictly monotonically increasing)
	@param value the input value to calculate the result for
	@param size number of values in arrays axis/value
	@param axis array containing the X values
	@param value array containing the Y values
	@return the interpolated output value from the map
 */
s32 MAP_S32_GetValue(u32 value, u8 size, s32 *axis, s32 *values) {
	s32 retval;
	u32 index;
	u32 axeval;
	u8  interval;

	if (value <= axis[0])
		retval = values[0];
	else {
		if (value >= axis[size-1])
			retval = values[size-1];
		else {
			/* find lower index (interval bisection) */
			interval = size/2;
			index = interval;
			do {
				interval /= 2;
				axeval = axis[index];
				if (value < axeval)
					index -= interval;
				else {
					if (value > axeval)
						index += interval;
					else
						interval = 0;
				}
			} while (interval > 0);
			/* interpolate */
			retval = MAP_S32_Interpol(value, axeval, axis[index+1], values[index], axis[index+1]);
		}
	}
	return retval;
}

/** Sine lookup table used for trigonometric functions */
const s32 sine_table[] = {
		(u32)(MATH_TRI_SCALE*0),                 (u32)(MATH_TRI_SCALE*0.0174524064372835),(u32)(MATH_TRI_SCALE*0.0348994967025010),
		(u32)(MATH_TRI_SCALE*0.0523359562429438),(u32)(MATH_TRI_SCALE*0.0697564737441253),(u32)(MATH_TRI_SCALE*0.0871557427476582),
		(u32)(MATH_TRI_SCALE*0.1045284632676535),(u32)(MATH_TRI_SCALE*0.1218693434051475),(u32)(MATH_TRI_SCALE*0.1391731009600654),
		(u32)(MATH_TRI_SCALE*0.1564344650402309),(u32)(MATH_TRI_SCALE*0.1736481776669303),(u32)(MATH_TRI_SCALE*0.1908089953765448),
		(u32)(MATH_TRI_SCALE*0.2079116908177593),(u32)(MATH_TRI_SCALE*0.2249510543438650),(u32)(MATH_TRI_SCALE*0.2419218955996677),
		(u32)(MATH_TRI_SCALE*0.2588190451025208),(u32)(MATH_TRI_SCALE*0.2756373558169992),(u32)(MATH_TRI_SCALE*0.2923717047227367),
		(u32)(MATH_TRI_SCALE*0.3090169943749474),(u32)(MATH_TRI_SCALE*0.3255681544571567),(u32)(MATH_TRI_SCALE*0.3420201433256687),
		(u32)(MATH_TRI_SCALE*0.3583679495453003),(u32)(MATH_TRI_SCALE*0.3746065934159120),(u32)(MATH_TRI_SCALE*0.3907311284892738),
		(u32)(MATH_TRI_SCALE*0.4067366430758002),(u32)(MATH_TRI_SCALE*0.4226182617406994),(u32)(MATH_TRI_SCALE*0.4383711467890774),
		(u32)(MATH_TRI_SCALE*0.4539904997395468),(u32)(MATH_TRI_SCALE*0.4694715627858908),(u32)(MATH_TRI_SCALE*0.4848096202463370),
		(u32)(MATH_TRI_SCALE*0.5),               (u32)(MATH_TRI_SCALE*0.5150380749100542),(u32)(MATH_TRI_SCALE*0.5299192642332050),
		(u32)(MATH_TRI_SCALE*0.5446390350150271),(u32)(MATH_TRI_SCALE*0.5591929034707468),(u32)(MATH_TRI_SCALE*0.5735764363510461),
		(u32)(MATH_TRI_SCALE*0.5877852522924731),(u32)(MATH_TRI_SCALE*0.6018150231520483),(u32)(MATH_TRI_SCALE*0.6156614753256583),
		(u32)(MATH_TRI_SCALE*0.6293203910498375),(u32)(MATH_TRI_SCALE*0.6427876096865393),(u32)(MATH_TRI_SCALE*0.6560590289905073),
		(u32)(MATH_TRI_SCALE*0.6691306063588582),(u32)(MATH_TRI_SCALE*0.6819983600624985),(u32)(MATH_TRI_SCALE*0.6946583704589973),
		(u32)(MATH_TRI_SCALE*0.7071067811865475),(u32)(MATH_TRI_SCALE*0.7193398003386511),(u32)(MATH_TRI_SCALE*0.7313537016191705),
		(u32)(MATH_TRI_SCALE*0.7431448254773942),(u32)(MATH_TRI_SCALE*0.7547095802227720),(u32)(MATH_TRI_SCALE*0.7660444431189780),
		(u32)(MATH_TRI_SCALE*0.7771459614569709),(u32)(MATH_TRI_SCALE*0.7880107536067220),(u32)(MATH_TRI_SCALE*0.7986355100472928),
		(u32)(MATH_TRI_SCALE*0.8090169943749474),(u32)(MATH_TRI_SCALE*0.8191520442889918),(u32)(MATH_TRI_SCALE*0.8290375725550417),
		(u32)(MATH_TRI_SCALE*0.8386705679454240),(u32)(MATH_TRI_SCALE*0.8480480961564260),(u32)(MATH_TRI_SCALE*0.8571673007021123),
		(u32)(MATH_TRI_SCALE*0.8660254037844386),(u32)(MATH_TRI_SCALE*0.8746197071393958),(u32)(MATH_TRI_SCALE*0.8829475928589269),
		(u32)(MATH_TRI_SCALE*0.8910065241883679),(u32)(MATH_TRI_SCALE*0.8987940462991670),(u32)(MATH_TRI_SCALE*0.9063077870366500),
		(u32)(MATH_TRI_SCALE*0.9135454576426009),(u32)(MATH_TRI_SCALE*0.9205048534524403),(u32)(MATH_TRI_SCALE*0.9271838545667874),
		(u32)(MATH_TRI_SCALE*0.9335804264972017),(u32)(MATH_TRI_SCALE*0.9396926207859084),(u32)(MATH_TRI_SCALE*0.9455185755993168),
		(u32)(MATH_TRI_SCALE*0.9510565162951536),(u32)(MATH_TRI_SCALE*0.9563047559630355),(u32)(MATH_TRI_SCALE*0.9612616959383189),
		(u32)(MATH_TRI_SCALE*0.9659258262890683),(u32)(MATH_TRI_SCALE*0.9702957262759965),(u32)(MATH_TRI_SCALE*0.9743700647852352),
		(u32)(MATH_TRI_SCALE*0.9781476007338056),(u32)(MATH_TRI_SCALE*0.9816271834476640),(u32)(MATH_TRI_SCALE*0.9848077530122081),
		(u32)(MATH_TRI_SCALE*0.9876883405951377),(u32)(MATH_TRI_SCALE*0.9902680687415703),(u32)(MATH_TRI_SCALE*0.9925461516413220),
		(u32)(MATH_TRI_SCALE*0.9945218953682733),(u32)(MATH_TRI_SCALE*0.9961946980917455),(u32)(MATH_TRI_SCALE*0.9975640502598242),
		(u32)(MATH_TRI_SCALE*0.9986295347545739),(u32)(MATH_TRI_SCALE*0.9993908270190957),(u32)(MATH_TRI_SCALE*0.9998476951563912),
		(u32)(MATH_TRI_SCALE*1)
};

/** Sine function.
	@param x angle in degree (e.g. 90�)
	@return sin(x)
*/
s32 MATH_Sine(s32 x) {
	s32 y;
	while (x >= 360)
		x-= 360;
	while (x < 0)
		x += 360;

	y = x % 90;

	if (x <  90)
		return  sine_table[y];
	else if (x < 180)
		return  sine_table[90-y];
	else if (x < 270)
		return -sine_table[y];
	return -sine_table[90-y];
}




/** Arctan lookup table.
	Calcutta code to create this table:
	MATH_TRI_ANG_2PI_LD = 30
	for(i=0;i<=MATH_TRI_ANG_2PI_LD;i++) { println(round(atan(2/(1<<i))/(2*PI)*(1<<MATH_TRI_ANG_2PI_LD))) }
*/
const s32 arctantab[] = {189202105,134217728,79233351,41864727,21251189,10666833,5338616,2669960,1335061,667541,333772,166886,83443,41722,20861,10430,5215,2608,1304,652,326,163,81,41,20,10,5,3,1,1,0,0};

#define MATH_ATAN_MAX_ITER 16 /** Number of iterations - must be <= MATH_TRI_ANG_2PI_LD, 16 is a good choice */

/** Cordic implementation of ATAN2.
	@param y y coordinate
	@param x x coordinate
	@result  angle in resolution MATH_TRI_ANG_2PI == (1<<MATH_TRI_ANG_2PI_LD) == 360�
*/
s32 Math_Atan2(s32 y, s32 x) {

	s32 alpha=0, yi, i;
	s32 *arctanptr = (s32*)arctantab;

	if (x==0) {
		if (y>0)
			return MATH_TRI_ANG_2PI/4; // 90�
		else // don't handle the invalid case X=0, Y=0, just return 270� as well
			return (s32)((3LL*MATH_TRI_ANG_PI)/2); // 270�
	}

	// reduce values by power of two until overflow can be avoided - unproven
	while ( ABS(x) > (1<<MATH_TRI_ANG_2PI_LD) || ABS(y) > (1<<MATH_TRI_ANG_2PI_LD) ) {
		x >>= 1;
		y >>= 1;
	}

	if (y < 0) {
		yi = y + (x << 1);
		x  = x - (y << 1);
		alpha -= *arctanptr;
	} else {
		yi = y - (x << 1);
		x  = x + (y << 1);
		alpha += *arctanptr;
	}
	y  = yi;
	arctanptr++;

	for (i = 0; i <= MATH_ATAN_MAX_ITER; i++) {
		if (y < 0) {
			yi = y + (x >> i);
			x  = x - (y >> i);
			alpha -= *arctanptr;
		} else {
			yi = y - (x >> i);
			x  = x + (y >> i);
			alpha += *arctanptr;
		}
		y  = yi;
		arctanptr++;
	}

	if (alpha < 0)
		alpha += (1<<MATH_TRI_ANG_2PI_LD);

	/* Note: the radius/hypotenuse can be derived from x by multiplying a scaling factor
	   0.2715717684432241 (0.6072529350088813 if the left shift part of the iteration was skipped).
	   We don't need it here, so we skip it, but it could be done without a costly 64bit multiplication
	   by scaling the factor to a percentage of 0x8000=100% and performing a division of high/low part */

	return alpha;
}

/** Square Root for integer values.
	@param n unsigned integer value to calculate square root from
	@return integer square root of n: sqrt(n)
 */
u32 Math_Sqrt(u32 n) {
	u32 root=0;
	u32 remainder=n;
	u32 place = 0x40000000; // u16: place = 0x4000, u8: place = 0x40
	u32 tmp;

	while (place > remainder)
		place >>= 2;
	while (place)  {
		tmp = root + place;
		if (remainder >= tmp) {
			remainder -= tmp;
			root += (place << 1);
		}
		root >>= 1;
		place >>= 2;
	}
	return root;
}

