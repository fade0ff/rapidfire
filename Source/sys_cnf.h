/** System configuration - Configuration
	Configuration of system resources (CPU clock, prescalers etc.)
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/


#ifndef SYS_CNF_H
#define SYS_CNF_H

/* Peripheral Enable */
#define SYSAHBCLKCTRL_CFG ( \
		SYS_CLK_SYS_CFG(1)   | SYS_CLK_ROM_CFG(1)   | SYS_CLK_RAM0_1_CFG(1) | SYS_CLK_FLASHREG_CFG(1) | \
		SYS_CLK_FLASH_CFG(1) | SYS_CLK_I2C0_CFG(0)  | SYS_CLK_GPIO_CFG(1)   | SYS_CLK_SWM_CFG(1)      | \
		SYS_CLK_SCT_CFG(0)   | SYS_CLK_WKT_CFG(0)   | SYS_CLK_MRT_CFG(1)    | SYS_CLK_SPI0_CFG(0)     | \
		SYS_CLK_SPI1_CFG(0)  | SYS_CLK_CRC_CFG(1)   | SYS_CLK_UART0_CFG(0)  | SYS_CLK_UART1_CFG(0)    | \
		SYS_CLK_UART2_CFG(0) | SYS_CLK_WWDT_CFG(1)  | SYS_CLK_IOCON_CFG(1)  | SYS_CLK_ACMP_CFG(0)     | \
		SYS_CLK_I2C0_CFG(1)  | SYS_CLK_I2C1_CFG(0)  | SYS_CLK_I2C2_CFG(0)   | SYS_CLK_ADC_CFG(0)      | \
		SYS_CLK_MTB_CFG(1)   | SYS_CLK_DMA_CFG(0))

/* Frequency configuration */
#define SYS_FRQ_CORE  12000000UL       ///! Desired core frequency 12 MHz - has to match SYS_FRQ_PLL and SYS_CLK_DIV
#define SYS_FRQ_OSC   12000000UL       ///! Oscillator/Crystal frequency */
#define SYS_FRQ_WDT   300000UL         ///! Desired Watchdog oscillator frequency */

/* Define source of main clock */
#define SYS_MAIN_CLKSEL_CFG SYSCON_CLKSRC_IRC

/* Use internal RC oscillator and/or external crystal (bypass not implemented) */
#define SYS_FRQ_USE_IRC     1          ///! 1: use internal oscillator, 2: don't use internal oscillator
#define SYS_FRQ_USE_SYSOSC  0          ///! 1: use main system oscillator, 2: don't use main system oscillator
#define SYS_FRQ_USE_WDT     1          ///! 1: use watchdog oscillator, 2: don't use watchdog oscillator

/* Enable PLLs */
#define SYS_FRQ_USE_SYS_PLL 0          ///! 1: use SYS PLL, 0: don't use SYS PLL

/* Setup PLL clock sources (only needed if enabled) */
#define SYS_PLL_CLKSRC_CFG             SYSCON_PLLSRC_IRC

/* PLL output frequency SYS_FRQ_PLL
   SYS_FRQ_PLL = SYS_FRQ_OSC*SYS_MSEL_CFG
*/
#define SYS_MSEL_CFG       5           ///! PLL multiplier [1..64]
#define SYS_PSEL_CFG       2           ///! PLL divider  [1,2,4,8]

/* The CPU core frequency is SYS_FRQ_PLL/SYS_CLK_DIV */
#define SYS_CLK_DIV        1           ///! CLock divider [1..256]

#define SYS_FRQ_WDT_FRQSEL SYSCON_WDT_FRQSEL_600KHZ ///! WDT analog frequency select
#define SYS_FRQ_WDT_DIV    2                        ///! WDT divider

/* Peripheral clock selects */
#define SYS_CLK_CLKOUT_CLKSEL_CFG      SYSCON_CLKOUTSRC_MAIN

/* Peripheral clock dividers 0..255 - 0 to disable */
#define SYS_CLK_UART_DIV      1
#define SYS_CLK_CLKOUT_DIV    0
#define SYS_CLK_IOCON_DIV0    1
#define SYS_CLK_IOCON_DIV1    0
#define SYS_CLK_IOCON_DIV2    0
#define SYS_CLK_IOCON_DIV3    0
#define SYS_CLK_IOCON_DIV4    0
#define SYS_CLK_IOCON_DIV5    0
#define SYS_CLK_IOCON_DIV6    0

#endif


