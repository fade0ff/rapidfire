/** DMA library
	Use of Direct Memory Access controller
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2017 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef DMA_H
#define DMA_H

#include "dma_cnf.h"

/** DMA entry table structure */
typedef struct dma_entry {
	u32 xfercfg;                                 ///! not used for entry table, only for reload descriptors
	u32 src_addr;                                ///! Source data end address
	u32 dst_addr;                                ///! Destination end address
	struct dma_entry *next;                      ///! link to next descriptor, 0 if none (16byte aligned)
} dma_entry_t;

#define DMA_NUM_CHANNELS    18                   ///! Number of DMA channels

/** Function pointer for DMA specific callback */
typedef void (*dma_callback_t)(void);


/* DMA input trigger input mux */
#define DMA_ITRIG_INMUX_ADC0_SEQA_IRQ   0x00
#define DMA_ITRIG_INMUX_ADC0_SEQB_IRQ   0x01
#define DMA_ITRIG_INMUX_SCT_DMA0        0x02
#define DMA_ITRIG_INMUX_SCT_DMA1        0x03
#define DMA_ITRIG_INMUX_ACMP_OUT        0x04
#define DMA_ITRIG_INMUX_PININT0         0x05
#define DMA_ITRIG_INMUX_PININT1         0x06
#define DMA_ITRIG_INMUX_DMA_INMUX0      0x07
#define DMA_ITRIG_INMUX_DMA_INMUX1      0x08
#define DMA_ITRIG_INMUX_DEFAULT         0x0F      ///! reset value

/** Set DMA input trigger input mux register.
	@param ch DMA channel (0..17)
	@param x value to set - use one of the DMA_ITRIG_INMUX_xxx macros
*/
#define DMA_SetItrigInmux(ch,x) { ((u32*)&LPC_DMATRIGMUX->DMA_ITRIG_INMUX0)[ch] = (u32)(x); }

/** Set DMA input trigger input mux register.
	@param x DMA input multiplexer [0..1]
	@param ch DMA channel (0..17)
*/
#define DMA_SetInmuxCh(x,ch) {  ((u32*)&LPC_INPUTMUX->DMA_INMUX_INMUX0 )[x]= (u32)(ch); }


/* CTRL */
#define DMA_CTRL_DISABLE     0                    ///! Controller disable
#define DMA_CTRL_ENABLE      1                    ///! Controller enable

/** Read DMA control register.
	@return value of control register
*/
#define DMA_GetCtrl()   (LPC_DMA->CTRL)

/** Set DMA control register.
	@param x value to set - use the DMA_CTRL_xxx macros
*/
#define DMA_SetCtrl(x)   {LPC_DMA->CTRL = (u32)(x);}

/* INTSTAT */
#define DMA_INTSTAT_ACTIVEINT    (1UL<<1)         ///! Interrupt pending
#define DMA_INTSTAT_ACTIVEERRINT (1UL<<2)         ///! Error interrupt pending

/** Read DMA interrupt status register.
	@return value of interrupt status register
*/
#define DMA_GetIntStatus()   (LPC_DMA->INTSTAT)


/* SRAMBASE */

/** Get base address of DMA descriptor table.
	@return base address of DMA descriptor table
*/
#define DMA_GetBaseAdr()   (LPC_DMA->SRAMBASE)

/** Set base address of DMA descriptor table.
	@param a base address of DMA descriptor table (512 byte aligned)
*/
#define DMA_SetBaseAdr(a)   {LPC_DMA->SRAMBASE = ((u32)(a) & 0xfffffe00);}


/* ENABLESET0 / ENABLECLR0 */

/** Read enable set register.
	@return bitfield of enabled channels
*/
#define DMA_GetEnable()   (LPC_DMA->ENABLESET0)

/** Write enable set register.
	@param x bitfield of channels to enable
*/
#define DMA_SetEnable(x)   {LPC_DMA->ENABLESET0 = (u32)(x);}

/** Write enable clear register.
	@param x bitfield of channels to disable
*/
#define DMA_ClrEnable(x)   {LPC_DMA->ENABLECLR0 = (u32)(x);}

/** Enable DMA channel.
	@param ch DMA channel (0..17)
	@param en 0: disable, else: enable
*/
#define DMA_EnableCh(ch, en)    { if ((en)!=0) {DMA_SetEnable(1UL<<(ch));} else {DMA_ClrEnable(1UL<<(ch));}}

/** Read enable state of a DMA channel.
	@param ch DMA channel (0..17)
	@return 0: disabled, else: enabled
*/
#define DMA_IsEnabledCh(ch) ( (LPC_DMA->ENABLESET0 & (1UL<<(ch))) != 0 )


/* ACTIVE0 */

/** Read active state register.
	@return bitfield of active channels
*/
#define DMA_GetActive()   (LPC_DMA->ACTIVE0)

/** Read active state of a DMA channel.
	@param ch DMA channel (0..17)
	@return 0: inactive, else: active
*/
#define DMA_IsActiveCh(ch) ( (LPC_DMA->ACTIVE0 & (1UL<<(ch))) != 0 )


/* BUSY0 */

/** Read busy state register.
	@return bitfield of busy channels
*/
#define DMA_GetBusy()   (LPC_DMA->BUSY0)

/** Read busy state of a DMA channel
	@param ch DMA channel (0..17)
	@return 0: inactive, else: active
*/
#define DMA_IsBusyCh(ch) ( (LPC_DMA->BUSY0 & (1UL<<(ch))) != 0 )


/* ERRINT0 */

/** Read error interrupt register.
	@return bitfield of error interrupt requests set for all channels
*/
#define DMA_GetErrInt()   (LPC_DMA->ERRINT0)

/** Write error interrupt register.
	@param x bitfield of error interrupt requests to clear (1 to clear)
*/
#define DMA_ClrErrInt(x)   {LPC_DMA->ERRINT0 = (u32)(x);}


/** Read error interrupt request of a DMA channel.
	@param ch DMA channel (0..17)
	@return 0: no error interrupt request, else: error interrupt request set
*/
#define DMA_GetErrIntCh(ch) ( (LPC_DMA->ERRINT0 & (1UL<<(ch))) != 0 )

/** Clear error interrupt request of a DMA channel.
	@param ch DMA channel (0..17)
*/
#define DMA_ClrErrIntCh(ch)   {LPC_DMA->ERRINT0 |= (1UL<<(ch));}


/* INTENSET0/INTENCLR0 */

/** Read interrupt enable set register.
	@return bitfield of enabled interrupts
*/
#define DMA_GetIntEnable()   (LPC_DMA->INTENSET0)

/** Write interrupt enable set register.
	@param x bitfield of interrupts to enable
*/
#define DMA_SetIntEnable(x)   {LPC_DMA->INTENSET0 = (u32)(x);}

/** Write interrupt enable clear register.
	@param x bitfield of interrupts to disable
*/
#define DMA_ClrIntEnable(x)   {LPC_DMA->INTENCLR0 = (u32)(x);}

/** Enable DMA channel interrupt.
	@param ch DMA channel (0..17)
	@param en 0: disable, else: enable
*/
#define DMA_IntEnableCh(ch, en)    { if ((en)!=0) {DMA_SetIntEnable(1UL<<(ch));} else {DMA_ClrIntEnable(1UL<<(ch));}}

/** Read interrupt enable state of a DMA channel.
	@param ch DMA channel (0..17)
	@return 0: disabled, else: enabled
*/
#define DMA_IsIntEnabledCh(ch) ( (LPC_DMA->INTENSET0 & (1UL<<(ch))) != 0 )


/* INTA0 */

/** Read interrupt A register.
	@return bitfield of interrupt requests set for all channels
*/
#define DMA_GetIntA()   (LPC_DMA->INTA0)

/** Write interrupt A register.
	@param x bitfield of interrupt requests to clear (1 to clear)
*/
#define DMA_ClrIntA(x)   {LPC_DMA->INTA0 = (u32)(x);}

/** Read interrupt A request of a DMA channel.
	@param ch DMA channel (0..17)
	@return 0: no interrupt request, else: interrupt request set
*/
#define DMA_GetIntACh(ch) ( (LPC_DMA->INTA0 & (1UL<<(ch))) != 0 )

/** Clear interrupt request of a DMA channel.
	@param ch DMA channel (0..17)
*/
#define DMA_ClrIntACh(ch)   {LPC_DMA->INTA0 |= (1UL<<(ch));}


/* INTB0 */

/** Read interrupt B register.
	@return bitfield of interrupt requests set for all channels
*/
#define DMA_GetIntB()   (LPC_DMA->INTB0)

/** Write interrupt B register.
	@param x bitfield of interrupt requests to clear (1 to clear)
*/
#define DMA_ClrIntB(x)   {LPC_DMA->INTB0 = (u32)(x);}

/** Read interrupt B request of a DMA channel.
	@param ch DMA channel (0..17)
	@return 0: no interrupt request, else: interrupt request set
*/
#define DMA_GetIntBCh(ch) ( (LPC_DMA->INTB0 & (1UL<<(ch))) != 0 )

/** Clear interrupt request of a DMA channel.
	@param ch DMA channel (0..17)
*/
#define DMA_ClrIntBCh(ch)   {LPC_DMA->INTB0 |= (1UL<<(ch));}


/* SETVALID0 */

/** Write set valid register.
	@param x bitfield of valid pending bits to set
*/
#define DMA_SetValid(x)   {LPC_DMA->SETVALID0 = (u32)(x);}

/** Set valid pending bit for a DMA channel.
	@param ch DMA channel (0..17)
*/
#define DMA_SetValidCh(ch)   {LPC_DMA->SETVALID0 |= (1UL<<(ch));}


/* SETTRIG0 */

/** Write set trigger register.
	@param x bitfield of trigger bits to set
*/
#define DMA_SetTrigger(x)   {LPC_DMA->SETTRIG0 = (u32)(x);}

/** Set trigger bit for a DMA channel.
	@param ch DMA channel (0..17)
*/
#define DMA_SetTriggerCh(ch)   {LPC_DMA->SETTRIG0 |= (1UL<<(ch));}


/* ABORT0 */

/** Write abort register.
	@param x bitfield of abort bits to set
*/
#define DMA_SetAbort(x)   {LPC_DMA->ABORT0 = (u32)(x);}

/** Set abort bit for a DMA channel.
	@param ch DMA channel (0..17)
*/
#define DMA_SetAbortCh(ch)   {LPC_DMA->ABORT0 |= (1UL<<(ch));}


/* CFGn */

#define DMA_CFG_PERIPHREQEN     (1UL << 0)       ///! Peripheral request enable
#define DMA_CFG_HWTRIGEN        (1UL << 1)       ///! Hardware triggering enable (via input multiplexer)
#define DMA_CFG_TRIGPOL_LO      0                ///! Hardware trigger polarity: active low or falling edge
#define DMA_CFG_TRIGPOL_HI      (1UL << 4)       ///! Hardware trigger polarity: active high or rising edge
#define DMA_CFG_TRIGTYPE_EDGE   0                ///! Hardware trigger is edge triggered
#define DMA_CFG_TRIGTYPE_LEVEL  (1UL << 5)       ///! Hardware trigger is level triggered
#define DMA_CFG_TRIGBURST_SNGL  0                ///! Hardware trigger causes a single transfer
#define DMA_CFG_TRIGBURST_BURST (1UL << 6)	     ///! Hardware trigger causes a burst transfer
#define DMA_CFG_BURSTPOWER_1    0                ///! Burst size: 1 transfer
#define DMA_CFG_BURSTPOWER_2    (1UL << 8)       ///! Burst size: 2 transfers
#define DMA_CFG_BURSTPOWER_4    (2UL << 8)       ///! Burst size: 4 transfers
#define DMA_CFG_BURSTPOWER_8    (3UL << 8)       ///! Burst size: 8 transfers
#define DMA_CFG_BURSTPOWER_16   (4UL << 8)       ///! Burst size: 16 transfers
#define DMA_CFG_BURSTPOWER_32   (5UL << 8)       ///! Burst size: 32 transfers
#define DMA_CFG_BURSTPOWER_64   (6UL << 8)       ///! Burst size: 64 transfers
#define DMA_CFG_BURSTPOWER_128  (7UL << 8)       ///! Burst size: 128 transfers
#define DMA_CFG_BURSTPOWER_256  (8UL << 8)       ///! Burst size: 256 transfers
#define DMA_CFG_BURSTPOWER_512  (9UL << 8)       ///! Burst size: 512 transfers
#define DMA_CFG_BURSTPOWER_1024 (10UL << 8)      ///! Burst size: 1024 transfers
#define DMA_CFG_BURSTPOWER(n)   (((x)&15) << 8)  ///! Set DMA burst size to 1<<x transfers [x=0..10]
#define DMA_CFG_SRCBURSTWRAP    (1UL << 14)      ///! Wrap source address
#define DMA_CFG_DSTBURSTWRAP    (1UL << 15)      ///! Wrap destination address
#define DMA_CFG_CHPRIORITY(p)   (((p)&7) << 16)  ///! Sets DMA channel priority [0..7], 0 is lowest

/** Write DMA channel config register.
	@param ch DMA channel (0..17)
	@param x value to set (construct from DMA_CFG_xxx macros)
*/
#define DMA_SetChConfig(ch,x)   {LPC_DMA->CHANNEL[ch].CFG = (u32)(x);}

/** Read DMA channel config register.
	@param ch DMA channel (0..17)
	@return value of channel config register
*/
#define DMA_GetChConfig(ch)    (LPC_DMA->DMACH[ch].CFG)


/* CTLSTATn */

#define DMA_CTLSTAT_VALIDPENDING (1UL<<0)
#define DMA_CTLSTAT_TRIG         (1UL<<2)


/** Read DMA channel control and status register.
	@param ch DMA channel (0..17)
	@return value of channel control and status register
*/
#define DMA_GetChCtlStat(ch)    (LPC_DMA->DMACH[ch].CTLSTAT)


/* XFERCFGn */

#define DMA_XFERCFG_CFGVALID     (1UL << 0)      ///! Configuration valid flag
#define DMA_XFERCFG_RELOAD       (1UL << 1)      ///! Reload the channel's control structure when the current descriptor is exhausted
#define DMA_XFERCFG_SWTRIG       (1UL << 2)      ///! Software trigger
#define DMA_XFERCFG_CLRTRIG      (1UL << 3)      ///! Clear Trigger. The trigger is cleared when this descriptor is exhausted.
#define DMA_XFERCFG_SETINTA      (1UL << 4)      ///! The INTA flag for this channel will be set when the current descriptor is exhausted
#define DMA_XFERCFG_SETINTB      (1UL << 5)      ///! The INTB flag for this channel will be set when the current descriptor is exhausted
#define DMA_XFERCFG_WIDTH_8      0               ///! 8-bit transfers are performed
#define DMA_XFERCFG_WIDTH_16     (1UL << 8)      ///! 16-bit transfers are performed
#define DMA_XFERCFG_WIDTH_32     (2UL << 8)      ///! 32-bit transfers are performed
#define DMA_XFERCFG_SRCINC_0     0               ///! The source address is not incremented for each transfer
#define DMA_XFERCFG_SRCINC_1     (1UL << 12)     ///! The source address is incremented by 1 for each transfer
#define DMA_XFERCFG_SRCINC_2     (2UL << 12)     ///! The source address is incremented by 2 for each transfer
#define DMA_XFERCFG_SRCINC_4     (3UL << 12)     ///! The source address is incremented by 2 for each transfer
#define DMA_XFERCFG_DSTINC_0     0               ///! The destination address is not incremented after a transfer
#define DMA_XFERCFG_DSTINC_1     (1UL << 14)     ///! The destination address is incremented by 1*width after each transfer
#define DMA_XFERCFG_DSTINC_2     (2UL << 14)     ///! The destination address is incremented by 2*width after each transfer
#define DMA_XFERCFG_DSTINC_4     (3UL << 14)     ///! The destination address is incremented by 4*width after each transfer
#define DMA_XFERCFG_XFERCOUNT(n) ((n-1) << 16)   ///! Total number of transfers to be performed [1..1024]


/** Read DMA channel transfer configuration register.
	@param ch DMA channel (0..17)
	@return value of channel transfer configuration register
*/
#define DMA_GetChXferConfig(ch)    (LPC_DMA->CHANNEL[ch].XFERCFG)

/** Read DMA channel transfer count.
	@param ch DMA channel (0..17)
	@return number of remaining transfers
*/
#define DMA_GetChXferCount(ch)     ((u16)(LPC_DMA->CHANNEL[ch].XFERCFG>>16)+1)

/** Write DMA channel transfer configuration register.
	@param ch DMA channel (0..17)
	@param x value to write to channel transfer configuration register
*/
#define DMA_SetChXferConfig(ch,x)  {LPC_DMA->CHANNEL[ch].XFERCFG = (u32)(x);}


/** Read DMA channel source address.
	@param ch DMA channel (0..17)
	@return source address
*/
#define DMA_GetChSrcAdr(ch) (dma_entry_table[ch].src_addr)

/** Write DMA channel source address.
	@param ch DMA channel (0..17)
	@param y source address
*/
#define DMA_SetChSrcAdr(ch,y) {dma_entry_table[ch].src_addr = (u32)(y);}

/** Read DMA channel destination address.
	@param ch DMA channel (0..17)
	@return destination address
*/
#define DMA_GetChDestAdr(ch) (dma_entry_table[ch].dst_addr)


/** Write DMA channel destination address.
	@param ch DMA channel (0..17)
	@param y destination address
*/
#define DMA_SetChDestAdr(ch,y) {dma_entry_table[ch].dst_addr = (y);}

/** Write DMA channel link address.
	@param ch DMA channel (0..17)
	@param y link address (16 byte aligned)
*/
#define DMA_SetChLinkAdr(ch,y) {dma_entry_table[ch].next = (y);}



/* Automatic macros */

/* -------------------------------------------- */

/* Channel access macros */
#define DMA_CHANNEL_0   0
#define DMA_CHANNEL_1   1
#define DMA_CHANNEL_2   2
#define DMA_CHANNEL_3   3
#define DMA_CHANNEL_4   4
#define DMA_CHANNEL_5   5
#define DMA_CHANNEL_6   6
#define DMA_CHANNEL_7   7
#define DMA_CHANNEL_8   8
#define DMA_CHANNEL_9   9
#define DMA_CHANNEL_10 10
#define DMA_CHANNEL_11 11
#define DMA_CHANNEL_12 12
#define DMA_CHANNEL_13 13
#define DMA_CHANNEL_14 14
#define DMA_CHANNEL_15 15
#define DMA_CHANNEL_16 16
#define DMA_CHANNEL_17 17

/* Channel access for fixed functions */
#define DMA_CH_USART0_RX     0
#define DMA_CH_USART0_TX     1
#define DMA_CH_USART1_RX     2
#define DMA_CH_USART1_TX     3
#define DMA_CH_USART2_RX     4
#define DMA_CH_USART2_TX     5
#define DMA_CH_SPI0_RX       6
#define DMA_CH_SPI0_TX       7
#define DMA_CH_SPI1_RX       8
#define DMA_CH_SPI1_TX       9
#define DMA_CH_I2C0_SLV_DMA 10
#define DMA_CH_I2C0_MST_DMA 11
#define DMA_CH_I2C1_SLV_DMA 12
#define DMA_CH_I2C1_MST_DMA 13
#define DMA_CH_I2C2_SLV_DMA 14
#define DMA_CH_I2C2_MST_DMA 15
#define DMA_CH_I2C3_SLV_DMA 16
#define DMA_CH_I2C3_MST_DMA 17

#define LPC_DMACH(x) (LPC_DMA.DMACH[x])          ///! Construct device pointer from channel number [x=0..17]

extern dma_entry_t dma_entry_table[DMA_NUM_CHANNELS];

/* Prototypes */

extern void DMA_Init(void);
extern void DMA_IRQEnable(u8 en);
extern void DMA_SetCallBack(u8 ch, dma_callback_t callback);

#endif
