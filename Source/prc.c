/** Processor library
	Provide abstraction on CPU level
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/
#include "global.h"

/** Interrupt nesting level */
volatile int prc_irq_nesting_lvl;

#if defined ( __CC_ARM   )
  #define __ASM            __asm                                      ///! asm keyword for ARM Compiler
  #define __INLINE         __inline                                   ///! inline keyword for ARM Compiler

#elif defined ( __ICCARM__ )
  #define __ASM           __asm                                       ///! asm keyword for IAR Compiler
  #define __INLINE        inline                                      ///! inline keyword for IAR Compiler. Only available in High optimization mode! */

#elif defined   (  __TASKING__  )
  #define __ASM            __asm                                      ///! asm keyword for TASKING Compiler
  #define __INLINE         inline                                     ///! inline keyword for TASKING Compiler

#else /* __GNUC__ */
  #define __ASM            __asm                                      ///! asm keyword for GNU Compiler
  #define __INLINE         inline                                     ///! inline keyword for GNU Compiler
  #define __packed __attribute__((__packed__))

#endif

