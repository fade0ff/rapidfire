/** DMA library - Configuration
	Configuration of Direct Memory Access controller
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef DMA_CNF_H
#define DMA_CNF_H

/* priority configuration 0 is lowest, 7 is highest */

/* @todo: default input multiplexer configuration */

#define DMA_PRIO_USART0_RX     3
#define DMA_PRIO_USART0_TX     3
#define DMA_PRIO_USART1_RX     2
#define DMA_PRIO_USART1_TX     2
#define DMA_PRIO_USART2_RX     0
#define DMA_PRIO_USART2_TX     0
#define DMA_PRIO_SPI0_RX       4
#define DMA_PRIO_SPI0_TX       4
#define DMA_PRIO_SPI1_RX       5
#define DMA_PRIO_SPI1_TX       5

#endif
