/** NVM library - Configuration
	Non volatile memory simulation implemented via flash
	LPC8xx ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2017 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef NVM_CNF_H
#define NVM_CNF_H

#define NVM_PAGE_SIZE   64           ///! number of bytes per NVRAM page

#define NVM_SECTOR_0    0x00007800   ///! start address of 1st NVRAM sector
#define NVM_SECTOR_1    0x00007C00   ///! start address of 2nd NVRAM sector
#define NVM_SECTOR_SIZE 1024         ///! size of NVRAM sector

#define NVM_MAGIC_PATTERN 0xdeadbeef ///! magic pattern to mark sector as active


#endif
