/** Synchronous Serial Library
	Communication via the SPI peripheral (i.e. SPI)
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#include "global.h"
#include "sys.h"
#include "prc.h"
#include "pin.h"
#include "spi.h"
#include "dma.h"
#include "int_cnf.h"

/** Array to translate physical SPI device number to virtual device number */
u8 spi_virtual_device_id[SPI_PHYS_DEVICE_NUM];
/** Device RAM structure - on per virtual device*/
spi_device_t spi_device_ram[SPI_VIRTUAL_DEVICE_NUM];
/** SPI queues - on per virtual device*/
spi_queue_t spi_queues[SPI_VIRTUAL_DEVICE_NUM][SPI_QUEUE_SIZE]; ///! SPI queues
/** Channel RAM structure - on per virtual device*/
spi_channel_t spi_channel_ram[SPI_CHANNEL_CONFIG_NUM];

#if SPI_USE_DMA
void SPI0_Handler(void);
void SPI1_Handler(void);

const u8 spi_dma_rx_ch_lookup[SPI_NUM_PHYS_DEVICES]   = { DMA_CH_SPI0_RX, DMA_CH_SPI1_RX };
const u8 spi_dma_tx_ch_lookup[SPI_NUM_PHYS_DEVICES]   = { DMA_CH_SPI0_TX, DMA_CH_SPI1_TX };
const u8 spi_dma_rx_prio_lookup[SPI_NUM_PHYS_DEVICES] = { DMA_PRIO_SPI0_RX, DMA_PRIO_SPI1_RX };
const u8 spi_dma_tx_prio_lookup[SPI_NUM_PHYS_DEVICES] = { DMA_PRIO_SPI0_TX, DMA_PRIO_SPI1_TX };
const dma_callback_t spi_dma_handler_lookup[SPI_NUM_PHYS_DEVICES] = {SPI0_Handler, SPI1_Handler};
#endif

/** Get virtual device identifier from channel number
	@param ch_id channel identifier
	@return virtual device identifier
*/
u8 SPI_GetDeviceID(u8 ch_id) {
	const spi_channel_cfg_t *ch_cfg_ptr = &spi_channel_cfg[ch_id];
	return ch_cfg_ptr->device_id;
}

/** Get pointer to physical device from channel number
	@param ch_id channel identifier
	@return pointer to physical device
*/
LPC_SPI_TypeDef* SPI_GetDevice(u8 ch_id) {
	const spi_channel_cfg_t *ch_cfg_ptr = &spi_channel_cfg[ch_id];
	return (LPC_SPI_TypeDef*)spi_device_cfg[ch_cfg_ptr->device_id].device;
}

/** Enable/disable SPI device
	@param device_id virtual device id
	@param enable 1: enable, 0: disable
 */
void SPI_Enable(u8 device_id, u8 enable) {
	LPC_SPI_TypeDef *pdevice = (LPC_SPI_TypeDef *)spi_device_cfg[device_id].device;

	PRC_Int_Disable();
	{
		u32 tmp = SPI_GetConfig(pdevice);
		if (enable != 0) {
			tmp |= SPI_CFG_ENABLE; // probably doesn't make sense anymore
			int n = SPI_GetInterrupt(pdevice);
			NVIC_SetPriority(n, SPI_IRQ_PRIO);
			NVIC_EnableIRQ(n);

		} else {
			tmp &= (u32)~SPI_CFG_ENABLE; // probably doesn't make sense anymore
			NVIC_DisableIRQ(SPI_GetInterrupt(pdevice));
		}
		SPI_SetConfig(pdevice, tmp);
	}
	PRC_Int_Enable();
}

/** Calculate divider from frequency.
	@param frequency  desired frequency
	@return 16bit divider value
 */
static inline u16 SPI_CalcTiming(u32 frequency) {
	u32 divval;
	divval = (u32)SYS_CLK_SPI_FRQ / frequency;
	if (divval > 1)
		divval -= 1;
	else if (divval > 0xffff)
		divval = 0xffff;
	return (u16)divval;
}

/** Start transfer of queued channel.
	@param queue pointer to queue entry
 */
static void SPI_StartQueuedCh(spi_queue_t *queue) {
	u8 ch_id = queue->ch_id;
	u8 device_id = spi_channel_cfg[ch_id].device_id;
	LPC_SPI_TypeDef* pdevice = (LPC_SPI_TypeDef*)spi_device_cfg[device_id].device;
	spi_channel_t* ch_ram_ptr = &spi_channel_ram[ch_id];

	if (queue->callback_start != NULL)
		(*queue->callback_start)();

	u16 config = spi_channel_cfg[ch_id].config;
	SPI_SetConfig(pdevice, config);

	u32 ctldat = spi_channel_cfg[ch_id].ctldat<<16;
	if (queue->rx_buf == NULL)
		ctldat |= SPI_TXCTL_RXIGNORE;
	if ( (queue->len == 1) && (queue->release_cs) )
		ctldat |= SPI_TXCTL_EOT;  // deassert HW CS after frame
	SPI_SetControl(pdevice, ctldat);
	ch_ram_ptr->ctldat = ctldat;
	SPI_SetDelay(pdevice, spi_channel_cfg[ch_id].delay);
	SPI_SetDivider(pdevice, ch_ram_ptr->div);

	// enable CS
	if (spi_channel_cfg[ch_id].cs_pin_ch != SPI_CS_HW)
		PIN_ChSet(spi_channel_cfg[ch_id].cs_pin_ch, SPI_CS_ACTIVE);

	// Start transfer
#if SPI_USE_DMA
	u32 xfer_width, xfer_inc,xfer_reload;
	if (ch_ram_ptr->bytesize==1) {
		xfer_width = DMA_XFERCFG_WIDTH_8;
	} else {
		xfer_width = DMA_XFERCFG_WIDTH_16;
	}
	// setup Rx DMA
	if (queue->rx_inc == 0)
		xfer_inc = 0;
	else
		xfer_inc = DMA_XFERCFG_DSTINC_1; // might not cover exotic cases where rx_inc != bytesize
	u8 phys_device_id;
	if (pdevice == LPC_SPI0)
		phys_device_id = 0;
	else
		phys_device_id = 1;
	u16 dma_rx_ch = spi_dma_rx_ch_lookup[phys_device_id];
	DMA_SetChDestAdr(dma_rx_ch,(u32)&queue->rx_buf[(queue->len-1)*ch_ram_ptr->bytesize]);
	DMA_SetChXferConfig(dma_rx_ch, xfer_width|xfer_inc|DMA_XFERCFG_CFGVALID|DMA_XFERCFG_SWTRIG/*?*/|DMA_XFERCFG_SETINTA/*?*/|DMA_XFERCFG_XFERCOUNT(queue->len));
	// setup Tx DMA
	if (queue->tx_inc == 0)
		xfer_inc = 0;
	else
		xfer_inc = DMA_XFERCFG_SRCINC_1; // might not cover exotic cases where rx_inc != bytesize
	u16 dma_tx_ch = spi_dma_tx_ch_lookup[phys_device_id];
	u16 tx_len;
	if ( (queue->len > 1) && (queue->release_cs) ) {
		// to release CS with last transfer, a link DMA  descriptor is used to write to TXDATCTL
		u16 data;
		if (ch_ram_ptr->bytesize==1)
			data = queue->tx_buf[queue->len-1];
		 else
			data = *(u16*)&queue->tx_buf[queue->len-2];
		ch_ram_ptr->ctldat |= SPI_TXCTL_EOF|data; // deassert HW CS after frame
		DMA_SetChLinkAdr(dma_tx_ch, &ch_ram_ptr->dma_link);
		tx_len = queue->len-1;
		xfer_reload = DMA_XFERCFG_RELOAD;
	} else {
		DMA_SetChLinkAdr(dma_tx_ch, 0);
		tx_len = queue->len;
		xfer_reload = 0;
	}
	DMA_SetChDestAdr(dma_tx_ch,(u32)&SPI_GetTxRegister(pdevice)); // previous linking could have overwritten the destination
	DMA_SetChSrcAdr(dma_tx_ch,(u32)&queue->tx_buf[(tx_len-1)*ch_ram_ptr->bytesize]);
	DMA_SetChXferConfig(dma_tx_ch, xfer_width|xfer_inc|xfer_reload|DMA_XFERCFG_CFGVALID|DMA_XFERCFG_SWTRIG|DMA_XFERCFG_XFERCOUNT(tx_len));
#else
	if (ch_ram_ptr->bytesize==1) {
		SPI_SetData(pdevice, queue->tx_buf[ch_ram_ptr->tx_idx]);
	} else {
		u16 *txptr = (u16*)&queue->tx_buf[ch_ram_ptr->tx_idx];
		SPI_SetData(pdevice, *txptr);
	}
	ch_ram_ptr->tx_idx+=queue->tx_inc;
	ch_ram_ptr->com_idx++;
#endif
}

/** Start transfer of given channel. Complex version.
	@param ch_id channel identifier
	@param tx_buf pointer to transmit buffer
	@param rx_buf pointer to receive buffer
	@param len number of transfers
	@param inc_tx number of bytes to increment pointer to transmit buffer
	@param inc_rx number of bytes to increment pointer to receiver buffer
	@param callback function pointer of callback function to call before the start of transfer
	@param callback function pointer of callback function to call at the end of transfer
	@return transfer status (SPI_TRANSFER_OK, SPI_TRANSFER_QUEUED, SPI_TRANSFER_FAILED)
 */
u8 SPI_StartTransferX(u8 ch_id, u8* tx_buf, u8* rx_buf, u16 len, u8 inc_tx, u8 inc_rx, u8 release_cs, spi_callback_t callback_start, spi_callback_t callback_end) {
	u8 device_id = spi_channel_cfg[ch_id].device_id;
	spi_device_t *dev_ram_ptr = &spi_device_ram[device_id];
	spi_channel_t* ch_ram_ptr = &spi_channel_ram[ch_id];
	spi_queue_t *q;
	u8 retval = 0;

	PRC_Int_Disable();
	{
		if (dev_ram_ptr->state == SPI_STATE_INACTIVE && dev_ram_ptr->queue_entries==0) {
			dev_ram_ptr->state = SPI_STATE_ACTIVE;
			dev_ram_ptr->active_ch = ch_id;
			retval = SPI_TRANSFER_OK;
		} else {
			// put channel in queue
			if (dev_ram_ptr->queue_entries < SPI_QUEUE_SIZE)
				retval = SPI_TRANSFER_QUEUED;
			else
				retval = SPI_TRANSFER_FAILED;
		}
		// Note: even a directly started transfer is using one queue entry to simplify parameter storage
	    // Therefore the queue_bottom_idx points to the current transfer (if there is one) and
	    // queue_entries is 1 even if there is actually no transfer queued.
		if (retval != SPI_TRANSFER_FAILED) {
			q = &spi_queues[device_id][(dev_ram_ptr->queue_bottom_idx+dev_ram_ptr->queue_entries)%SPI_QUEUE_SIZE];
			q->callback_start = callback_start;
			q->callback_end = callback_end;
			q->ch_id  = ch_id;
			q->tx_buf = tx_buf;
			q->rx_buf = rx_buf;
			q->len    = len;
			q->rx_inc = inc_rx;
			q->tx_inc = inc_tx;
#if SPI_USE_DMA == 0
			ch_ram_ptr->com_idx = 0;
			ch_ram_ptr->tx_idx = 0;
			ch_ram_ptr->rx_idx = 0;
#endif
			q->release_cs = release_cs;
			dev_ram_ptr->queue_entries++;
			ch_ram_ptr->pending_count++;
			if (retval == SPI_TRANSFER_OK)
				SPI_StartQueuedCh(q);
		}
	}
	PRC_Int_Enable();

	return retval;
}

/** Start transfer of given channel. Simplified version.
	@param ch_id channel identifier
	@param tx_buf pointer to transmit buffer
	@param rx_buf pointer to receive buffer
	@param len number of transfers
	@param callback function pointer of callback function to call at the end of transfer
	@return transfer status (SPI_TRANSFER_OK, SPI_TRANSFER_QUEUED, SPI_TRANSFER_FAILED)
 */
u8 SPI_StartTransfer(u8 ch_id, u8* tx_buf, u8* rx_buf, u16 len, spi_callback_t callback_end) {
	u8 bytesize = spi_channel_ram[ch_id].bytesize;
	return SPI_StartTransferX(ch_id, tx_buf, rx_buf, len, bytesize, bytesize, SPI_CS_RELEASE, NULL, callback_end);
}


/** Interrupt handler called at the end of transmission.
	@param device_id virtual device id
*/
static void SPI_TransferHandler(u8 device_id) {
	LPC_SPI_TypeDef* pdevice = (LPC_SPI_TypeDef*)spi_device_cfg[device_id].device;
	SPI_SetIntEnable(pdevice, 0);
	spi_device_t  *dev_ram_ptr  = &spi_device_ram[device_id];
	spi_queue_t   *queue = &spi_queues[device_id][dev_ram_ptr->queue_bottom_idx];
	u8 ch_id = dev_ram_ptr->active_ch;
	spi_channel_t *ch_ram_ptr   = &spi_channel_ram[ch_id];

#if SPI_USE_DMA == 0
	// receive RX bytes
	if (queue->rx_buf != NULL) {
		if (ch_ram_ptr->bytesize==1) {
			queue->rx_buf[ch_ram_ptr->rx_idx] = SPI_GetData(pdevice);
		} else {
			u16 *rxptr = (u16*)&queue->rx_buf[ch_ram_ptr->rx_idx];
			*rxptr = SPI_GetData(pdevice);
		}
		ch_ram_ptr->rx_idx+=queue->rx_inc;
	}

	// check if there's more to transmit
	if (ch_ram_ptr->com_idx < queue->len) {
		if ( (ch_ram_ptr->com_idx = queue->len -1) && (queue->release_cs) )
			SPI_SetControl(pdevice, ch_ram_ptr->ctldat|SPI_TXCTL_EOF); // set EOF for last transfer
		if (ch_ram_ptr->bytesize==1) {
			SPI_SetData(pdevice, queue->tx_buf[ch_ram_ptr->tx_idx]);
		} else {
			u16 *txptr = (u16*)&queue->tx_buf[ch_ram_ptr->tx_idx];
			SPI_SetData(pdevice, *txptr);
		}
		ch_ram_ptr->tx_idx+=queue->tx_inc;
		ch_ram_ptr->com_idx++;
	} else {
#endif
		// Queue entry finished -> disable CS
		if (spi_channel_cfg[ch_id].cs_pin_ch != SPI_CS_HW && (queue->release_cs == SPI_CS_RELEASE))
			PIN_ChSet(spi_channel_cfg[ch_id].cs_pin_ch, SPI_CS_INACTIVE);

		if (queue->callback_end != NULL)    // still points to old queue position
			(*queue->callback_end)();

		// schedule >
		if (dev_ram_ptr->queue_entries > 0)
			dev_ram_ptr->queue_entries--;
		if (dev_ram_ptr->queue_entries > 0) {
			// start next transmission
			dev_ram_ptr->queue_bottom_idx = (dev_ram_ptr->queue_bottom_idx + 1)%SPI_QUEUE_SIZE;
			ch_id = spi_queues[device_id][dev_ram_ptr->queue_bottom_idx].ch_id;
			dev_ram_ptr->active_ch = ch_id;
			SPI_StartQueuedCh(&spi_queues[device_id][dev_ram_ptr->queue_bottom_idx]);
		} else
			dev_ram_ptr->state = SPI_STATE_INACTIVE;
		// <schedule

		ch_ram_ptr->pending_count--;        // still points to old channel
#if SPI_USE_DMA == 0
	}
#endif
}

/** SPI Initialization. Call once after reset before using any other SPI function. */
void SPI_Init(void) {
	int device_id, ch_id;

	for (ch_id=0; ch_id<SPI_CHANNEL_CONFIG_NUM; ch_id++) {
		spi_channel_t* ch_ram_ptr = &spi_channel_ram[ch_id];

		u32 frq   = spi_channel_cfg[ch_id].frequency;
		device_id = spi_channel_cfg[ch_id].device_id;
		ch_ram_ptr->div = SPI_CalcTiming(frq);
		ch_ram_ptr->pending_count = 0;
		if ( ((spi_channel_cfg[ch_id].ctldat>>8) & 0x0f) > 7)
			ch_ram_ptr->bytesize = 2;
		else
			ch_ram_ptr->bytesize = 1;
#if SPI_USE_DMA
		LPC_SPI_TypeDef *pdevice = spi_device_cfg[device_id].device;
		ch_ram_ptr->dma_link.xfercfg = DMA_XFERCFG_WIDTH_32|DMA_XFERCFG_CFGVALID|DMA_XFERCFG_SWTRIG|DMA_XFERCFG_XFERCOUNT(1);
		ch_ram_ptr->dma_link.src_addr = (u32)&ch_ram_ptr->ctldat;
		ch_ram_ptr->dma_link.dst_addr = (u32)&SPI_GetTxDataCtlRegister(pdevice);
		ch_ram_ptr->dma_link.next = 0;
#endif
	}

	for (device_id=0; device_id<SPI_VIRTUAL_DEVICE_NUM; device_id++) {
		LPC_SPI_TypeDef *pdevice = spi_device_cfg[device_id].device;
		u8 phys_device_id;
		if (pdevice == LPC_SPI0)
			phys_device_id = 0;
		else
			phys_device_id = 1;
		spi_virtual_device_id[phys_device_id] = device_id;
#if SPI_USE_DMA
		// enable receive DMA
		u16 dma_rx_ch = spi_dma_rx_ch_lookup[phys_device_id];
		DMA_EnableCh(dma_rx_ch,1);
		DMA_SetChConfig(dma_rx_ch, DMA_CFG_PERIPHREQEN|DMA_CFG_CHPRIORITY(spi_dma_rx_prio_lookup[phys_device_id]));
		DMA_SetChSrcAdr(dma_rx_ch,(u32)&SPI_RxRegister(pdevice));
		DMA_SetChLinkAdr(dma_rx_ch, 0);
		DMA_SetCallBack(dma_rx_ch, spi_dma_handler_lookup[phys_device_id]);
		// enable transmit DMA
		u16 dma_tx_ch = spi_dma_tx_ch_lookup[device_id];
		DMA_EnableCh(dma_tx_ch,1);
		DMA_SetChConfig(dma_tx_ch, DMA_CFG_PERIPHREQEN|DMA_CFG_CHPRIORITY(spi_dma_tx_prio_lookup[phys_device_id]));
		DMA_SetChDestAdr(dma_tx_ch,(u32)&SPI_GetTxRegister(pdevice));
		DMA_SetChLinkAdr(dma_tx_ch, 0);
		DMA_SetIntEnable(1<<dma_rx_ch);
#endif
		SPI_Enable(device_id, 1);
	}
}

/** Get number of pending transfers for the given channel.
	@param ch_id channel identifier
	@return number of pending transfers
*/
u8 SPI_GetChPendingCnt(u8 ch_id) {
	return spi_channel_ram[ch_id].pending_count;
}

#if SPI_USE_DMA

void SPI0_Handler(void) {
	SPI_TransferHandler(spi_virtual_device_id[0]);
}

void SPI1_Handler(void) {
	SPI_TransferHandler(spi_virtual_device_id[1]);
}


#else

void SPI0_IRQHandler(void) {
	SPI_TransferHandler(spi_virtual_device_id[0]);
}

void SPI1_IRQHandler(void) {
	SPI_TransferHandler(spi_virtual_device_id[1]);
}
#endif
