/** In application programming
	Definitions for calling IAP ROM functions
	LPC824 ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2017 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#include "global.h"
#include "iap.h"
#include "prc.h"

#define IAP_PARAM_CNT  5
#define IAP_RESULT_CNT 5

u32 iap_command_param[IAP_PARAM_CNT];
u32 iap_status_result[IAP_RESULT_CNT];
IAP iap_entry = (IAP) IAP_LOCATION;

/**
   Prepare flash sectors for writing.
   @param start_sector Number of start sector
   @param end_sector   Number of end sector (>= start_sector)
   @return Status
 */
u8 iap_prepare_sectors(u8 start_sector, u8 end_sector) {
	u8 status;
	PRC_Int_Disable();
	iap_command_param[0] = IAP_CMD_PREPARE_SECTOR;
	iap_command_param[1] = start_sector;
	iap_command_param[2] = end_sector;
	iap_entry(iap_command_param, iap_status_result);
	status = (u8)iap_status_result[0];
	PRC_Int_Enable();
	return status;
}


/**
   Copy from RAM to flash.
   @param src_adr Address to copy from (dword aligned)
   @param dst_adr Address to copy to (dword aligned)
   @param count Number of bytes to be written (64,128,255,512 or 1024)
   @return Status
 */
u8 iap_copy_to_flash(u32 src_adr, u32 dst_adr, u16 count) {
	u8 status;
	PRC_Int_Disable();
	iap_command_param[0] = IAP_CMD_COPY_RAM_TO_FLASH;
	iap_command_param[1] = dst_adr;
	iap_command_param[2] = src_adr;
	iap_command_param[3] = count;
	iap_command_param[4] = 0;
	iap_entry(iap_command_param, iap_status_result);
	status = (u8)iap_status_result[0];
	PRC_Int_Enable();
	return status;
}

/**
   Erase flash sectors.
   @param start_sector Number of start sector
   @param end_sector   Number of end sector (>= start_sector)
   @return Status
 */
u8 iap_erase_sectors(u8 start_sector, u8 end_sector) {
	u8 status;
	PRC_Int_Disable();
	iap_command_param[0] = IAP_CMD_ERASE_SECTOR;
	iap_command_param[1] = start_sector;
	iap_command_param[2] = end_sector;
	iap_command_param[3] = 0;
	iap_entry(iap_command_param, iap_status_result);
	status = (u8)iap_status_result[0];
	PRC_Int_Enable();
	return status;
}

/**
   Blank check flash sectors.
   @param start_sector Number of start sector
   @param end_sector   Number of end sector (>= start_sector)
   @param offset Pointer to store offset of the first non blank word location if the Status Code is IAP_STAT_SECTOR_NOT_BLANK.
   @param contents Pointer to store contents of non blank word location.
   @return Status
 */
u8 iap_blank_check_sectors(u8 start_sector, u8 end_sector, u32 *offset, u32 *contents) {
	u8 status;
	PRC_Int_Disable();
	iap_command_param[0] = IAP_CMD_BLANK_CHECK_SECTOR;
	iap_command_param[1] = start_sector;
	iap_command_param[2] = end_sector;
	iap_entry(iap_command_param, iap_status_result);
	status = (u8)iap_status_result[0];
	if (status == IAP_STAT_SECTOR_NOT_BLANK) {
		*offset = iap_status_result[1];
		*contents = iap_status_result[2];
	}
	PRC_Int_Enable();
	return status;
}

/**
   Read Part Identification number.
   @param id Pointer to store 32bit ID
   @return Status
 */
u8 iap_read_part_id(u32 *id) {
	u8 status;
	PRC_Int_Disable();
	iap_command_param[0] = IAP_CMD_READ_PART_ID;
	iap_entry(iap_command_param, iap_status_result);
	status = (u8)iap_status_result[0];
	if (status == IAP_STAT_CMD_SUCCESS) {
		*id = iap_status_result[1];
	}
	PRC_Int_Enable();
	return status;
}

/**
   Read boot code version number.
   @param id Pointer to store 1bit boot code version number
   @return Status
 */
u8 iap_read_boot_version(u16 *version) {
	u8 status;
	PRC_Int_Disable();
	iap_command_param[0] = IAP_CMD_READ_BOOT_VERSION;
	iap_entry(iap_command_param, iap_status_result);
	status = (u8)iap_status_result[0];
	if (status == IAP_STAT_CMD_SUCCESS) {
		*version = (u16)iap_status_result[1];
	}
	PRC_Int_Enable();
	return status;
}

/**
   Compare.
   @param src_adr Address to compare (dword aligned)
   @param dst_adr Address to compare (dword aligned)
   @param count Number of bytes to be compared (multiple of 4)
   @param Pointer to store offset of the first mismatch if the status code is IAP_STAT_SRC_DEST_EQUAL
   @return Status
 */
u8 iap_compare(u32 src_adr, u32 dst_adr, u16 count, u32* offset) {
	u8 status;
	PRC_Int_Disable();
	iap_command_param[0] = IAP_CMD_COMPARE;
	iap_command_param[1] = dst_adr;
	iap_command_param[2] = src_adr;
	iap_command_param[3] = count;
	iap_entry(iap_command_param, iap_status_result);
	status = (u8)iap_status_result[0];
	if (status == IAP_STAT_SRC_DEST_EQUAL) {
		*offset = (u16)iap_status_result[1];
	}
	PRC_Int_Enable();
	return status;
}

/**
   Invoke the bootloader in ISP mode.
   @return Status
 */
u8 iap_invoke_isp(void) {
	u8 status;
	PRC_Int_Disable();
	iap_command_param[0] = IAP_CMD_REINVOKE_ISP;
	iap_entry(iap_command_param, iap_status_result);
	status = (u8)iap_status_result[0];
	PRC_Int_Enable();
	return status;
}

/**
   Read UID.
   @param uid Array of 4 dwords to store UID
   @return Status
 */
u8 iap_read_uid(u32 *uid) {
	u8 status;
	PRC_Int_Disable();
	iap_command_param[0] = IAP_CMD_PREPARE_SECTOR;
	iap_entry(iap_command_param, iap_status_result);
	status = (u8)iap_status_result[0];
	if (status == IAP_STAT_CMD_SUCCESS) {
		uid[0] = iap_status_result[1];
		uid[1] = iap_status_result[2];
		uid[3] = iap_status_result[3];
		uid[4] = iap_status_result[4];
	}
	PRC_Int_Enable();
	return status;
}

/**
   Erase flash pages.
   @param start_page Number of start page
   @param end_page   Number of end page (>= start_page)
   @return Status
 */
u8 iap_erase_pages(u8 start_page, u8 end_page) {
	u8 status;
	PRC_Int_Disable();
	iap_command_param[0] = IAP_CMD_ERASE_PAGE;
	iap_command_param[1] = start_page;
	iap_command_param[2] = end_page;
	iap_command_param[3] = 0;
	iap_entry(iap_command_param, iap_status_result);
	status = (u8)iap_status_result[0];
	PRC_Int_Enable();
	return status;
}
