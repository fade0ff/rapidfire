/** Pin Access
	Configure pin function / direction / settings, read input pins, write output pins
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef PIN_H
#define PIN_H

#include "pin_cnf.h"

/*
Package		GPIO Port 0			GPIO Port 1			GPIO Port 2
LQFP48		PIO0_0 to PIO0_29	-					-
LQFP64		PIO0_0 to PIO0_31	PIO1_0 to PIO1_11	-
LQFP100		PIO0_0 to PIO0_31	PIO1_0 to PIO1_31	PIO2_0 to PIO2_11
 */

#define LPC_GPIO LPC_GPIO_PORT

/* Pin access via channel identifiers */

/** Get port from channel identifier c */
#define PIN_CH_PORT(ch)   ( (u8)((ch) >> 5) )
/** Get pin from channel identifier c */
#define PIN_CH_PIN(ch)    ( (ch) & 0x1f )
/** Create channel identifier from port and pin */
#define PIN_CH(port,pin) (((port)<<5) | (pin) )


/** Set pin direction through channel identifier.
	@param ch pin channel identifier (created via PIN_CH)
	@param level 0:lo or 1:hi
 */
#define PIN_ChSet(ch, value) {                \
	if ((value)==0)                           \
		LPC_GPIO->CLR0 = 1UL<<PIN_CH_PIN(ch); \
	else                                      \
		LPC_GPIO->SET0 = 1UL<<PIN_CH_PIN(ch); \
}

/** Read pin input through channel identifier.
	@param ch pin channel identifier (created via PIN_CH)
	@return level 0:lo or 1:hi
 */
#define PIN_ChGet(ch) (((LPC_GPIO->PIN0 & (1UL<<PIN_CH_PIN(ch)) ) != 0)?1:0)

/** Set pin direction through channel identifier.
	@param ch pin channel identifier (created via PIN_CH)
	@param dir 0 for input, 1 for output
 */
#define PIN_ChSetDir(ch, dir) {                        \
	if ((dir)==0)                                      \
		LPC_GPIO->DIR0 &= (u32)~(1UL<<PIN_CH_PIN(ch)); \
	else                                               \
		LPC_GPIO->DIR0 |= (1UL<<PIN_CH_PIN(ch));       \
}


/** Set Pin interrupt select register.
	@param x pin interrupt [0..7]
	@param y pin channel
 */
#define PIN_InterruptSelect(x, y) { LPC_INMUX->PINTSEL[x] = (y); }


/* Pin interrupt mode register */
#define PIN_ISEL_EDGE(x)  0                    ///! Edge sensitive pin interrupt x=0..7
#define PIN_ISEL_LEVEL(x) (1UL<<(x))           ///! Level sensitive pin interrupt x=0..7

/** Get Pin interrupt mode register.
	@return ISEL register]
 */
#define PIN_GetInterruptMode() ( LPC_GPIO_PIN_INT->ISEL)

/** Set Pin interrupt mode register.
	@param x value to write to ISEL register (use PIN_ISEL_xxx)]
 */
#define PIN_SetInterruptMode(x) { LPC_GPIO_PIN_INT->ISEL = (x); }

#define PIN_INT(x) (1UL<<(x))                  ///! Pin interrupt x=0..7

/** Enable rising edge or level interrupt.
	@param x mask of interrupt to enable (use PIN_INT_xxx)]
	@param en enable (0: disable, 1: enable)
 */
#define PIN_EnableRisingEdgeInt(x,en) { if (en==0) LPC_GPIO_PIN_INT->CIENR = (x); else LPC_GPIO_PIN_INT->SIENR = (x); }

/** Enable falling edge interrupt or set active level.
	@param x mask of interrupt to enable (use PIN_INT_xxx)]
	@param en enable (0: disable, 1: enable)
 */
#define PIN_EnableFallingEdgeInt(x,en) { if (en==0) LPC_GPIO_PIN_INT->CIENF = (x); else LPC_GPIO_PIN_INT->SIENF = (x); }

/** Get rising edge detection for pin interrupt.
	@return value of RISE register (set bits mean detected edges)
 */
#define PIN_GetRisingEdgeDetect(x) ( LPC_GPIO_PIN_INT->RISE )

/** Clear rising edge detection for pin interrupt.
    Writing a one clears the detection.
	@param x value to write to RISE register (use PIN_INT_xxx)]
 */
#define PIN_ClrRisingEdgeDetect(x) { LPC_GPIO_PIN_INT->RISE = (x); }

/** Get falling edge detection for pin interrupt.
	@return value of RISE register (set bits mean detected edges)
 */
#define PIN_GetFallingEdgeDetect(x) ( LPC_GPIO_PIN_INT->FALL )

/** Clear falling edge detection for pin interrupt.
    Writing a one clears the detection.
	@param x value to write to FALL register (use PIN_INT_xxx)]
 */
#define PIN_ClrFallingEdgeDetect(x) { LPC_GPIO_PIN_INT->FALL = (x); }


/** Get interrupt status.
	@return value of IST register (set bits mean interrupt requests)
 */
#define PIN_GetIntStatus(x) ( LPC_GPIO_PIN_INT->IST )

/** Clear edge interrupt request for pin interrupt.
    Writing a one clears the interrupt request in edge mode and toggles active edge in level mode.
	@param x value to write to FALL register (use PIN_INT_xxx)]
 */
#define PIN_ClrIntStatus(x) { LPC_GPIO_PIN_INT->IST = (x); }

/* Channel definitions */

#define PIN_0_0  PIN_CH(0, 0)
#define PIN_0_1  PIN_CH(0, 1)
#define PIN_0_2  PIN_CH(0, 2)
#define PIN_0_3  PIN_CH(0, 3)
#define PIN_0_4  PIN_CH(0, 4)
#define PIN_0_5  PIN_CH(0, 5)
#define PIN_0_6  PIN_CH(0, 6)
#define PIN_0_7  PIN_CH(0, 7)
#define PIN_0_8  PIN_CH(0, 8)
#define PIN_0_9  PIN_CH(0, 9)
#define PIN_0_10 PIN_CH(0,10)
#define PIN_0_11 PIN_CH(0,11)
#define PIN_0_12 PIN_CH(0,12)
#define PIN_0_13 PIN_CH(0,13)
#define PIN_0_14 PIN_CH(0,14)
#define PIN_0_15 PIN_CH(0,15)
#define PIN_0_16 PIN_CH(0,16)
#define PIN_0_17 PIN_CH(0,17)
#define PIN_0_18 PIN_CH(0,18)
#define PIN_0_19 PIN_CH(0,19)
#define PIN_0_20 PIN_CH(0,20)
#define PIN_0_21 PIN_CH(0,21)
#define PIN_0_22 PIN_CH(0,22)
#define PIN_0_23 PIN_CH(0,23)
#define PIN_0_24 PIN_CH(0,24)
#define PIN_0_25 PIN_CH(0,25)
#define PIN_0_26 PIN_CH(0,26)
#define PIN_0_27 PIN_CH(0,27)
#define PIN_0_28 PIN_CH(0,28)

#define PIN_NA 0xff


/* Port channel access macros */
#define PIN_PORT_0 0
#define PIN_PORT_1 1

/** Pin Initialization - configures all pins as specified in pin_cnf.h
 *  Needs to be changed for LQFP66 and LQFP100 (PIO[1][x] and PIO[2][x]).
 */
#define PIN_Init() {                                     \
		LPC_GPIO->PIN0           = PIN_PORT0_LVL_CFG;    \
		LPC_IOCON->PIO0_0        = PIN_IOCON_P0_0_CFG;   \
		LPC_IOCON->PIO0_1        = PIN_IOCON_P0_1_CFG;   \
		LPC_IOCON->PIO0_2        = PIN_IOCON_P0_2_CFG;   \
		LPC_IOCON->PIO0_3        = PIN_IOCON_P0_3_CFG;   \
		LPC_IOCON->PIO0_4        = PIN_IOCON_P0_4_CFG;   \
		LPC_IOCON->PIO0_5        = PIN_IOCON_P0_5_CFG;   \
		LPC_IOCON->PIO0_6        = PIN_IOCON_P0_6_CFG;   \
		LPC_IOCON->PIO0_7        = PIN_IOCON_P0_7_CFG;   \
		LPC_IOCON->PIO0_8        = PIN_IOCON_P0_8_CFG;   \
		LPC_IOCON->PIO0_9        = PIN_IOCON_P0_9_CFG;   \
		LPC_IOCON->PIO0_10       = PIN_IOCON_P0_10_CFG;  \
		LPC_IOCON->PIO0_11       = PIN_IOCON_P0_11_CFG;  \
		LPC_IOCON->PIO0_12       = PIN_IOCON_P0_12_CFG;  \
		LPC_IOCON->PIO0_13       = PIN_IOCON_P0_13_CFG;  \
		LPC_IOCON->PIO0_14       = PIN_IOCON_P0_14_CFG;  \
		LPC_IOCON->PIO0_15       = PIN_IOCON_P0_15_CFG;  \
		LPC_IOCON->PIO0_16       = PIN_IOCON_P0_16_CFG;  \
		LPC_IOCON->PIO0_17       = PIN_IOCON_P0_17_CFG;  \
		LPC_IOCON->PIO0_18       = PIN_IOCON_P0_18_CFG;  \
		LPC_IOCON->PIO0_19       = PIN_IOCON_P0_19_CFG;  \
		LPC_IOCON->PIO0_20       = PIN_IOCON_P0_20_CFG;  \
		LPC_IOCON->PIO0_21       = PIN_IOCON_P0_21_CFG;  \
		LPC_IOCON->PIO0_22       = PIN_IOCON_P0_22_CFG;  \
		LPC_IOCON->PIO0_23       = PIN_IOCON_P0_23_CFG;  \
		LPC_IOCON->PIO0_24       = PIN_IOCON_P0_24_CFG;  \
		LPC_IOCON->PIO0_25       = PIN_IOCON_P0_25_CFG;  \
		LPC_IOCON->PIO0_26       = PIN_IOCON_P0_26_CFG;  \
		LPC_IOCON->PIO0_27       = PIN_IOCON_P0_27_CFG;  \
		LPC_IOCON->PIO0_28       = PIN_IOCON_P0_28_CFG;  \
		LPC_GPIO->DIR0           = PIN_PORT0_DIR_CFG;    \
		LPC_SWM->PINASSIGN[0]    = PIN_PINASSIGN0_CFG;   \
		LPC_SWM->PINASSIGN[1]    = PIN_PINASSIGN1_CFG;   \
		LPC_SWM->PINASSIGN[2]    = PIN_PINASSIGN2_CFG;   \
		LPC_SWM->PINASSIGN[3]    = PIN_PINASSIGN3_CFG;   \
		LPC_SWM->PINASSIGN[4]    = PIN_PINASSIGN4_CFG;   \
		LPC_SWM->PINASSIGN[5]    = PIN_PINASSIGN5_CFG;   \
		LPC_SWM->PINASSIGN[6]    = PIN_PINASSIGN6_CFG;   \
		LPC_SWM->PINASSIGN[7]    = PIN_PINASSIGN7_CFG;   \
		LPC_SWM->PINASSIGN[8]    = PIN_PINASSIGN8_CFG;   \
		LPC_SWM->PINASSIGN[9]    = PIN_PINASSIGN9_CFG;   \
		LPC_SWM->PINASSIGN[10]   = PIN_PINASSIGN10_CFG;  \
		LPC_SWM->PINASSIGN[11]   = PIN_PINASSIGN11_CFG;  \
		LPC_SWM->PINENABLE0      = PIN_PINENABLE0_CFG;   \
}



/* Note: everything below this point is just to automatically create the macros for PIN_Init() */
#define PIN_PORT0_LVL_CFG (PIN_P0_0_LVL_CFG      |(PIN_P0_1_LVL_CFG<<1)  |(PIN_P0_2_LVL_CFG<<2)  |(PIN_P0_3_LVL_CFG<<3)  |(PIN_P0_4_LVL_CFG<<4)  |(PIN_P0_5_LVL_CFG<<5)  |(PIN_P0_6_LVL_CFG<<6)   | \
                          (PIN_P0_7_LVL_CFG<<7)  |(PIN_P0_8_LVL_CFG<<8)  |(PIN_P0_9_LVL_CFG<<9)  |(PIN_P0_10_LVL_CFG<<10)|(PIN_P0_11_LVL_CFG<<11)|(PIN_P0_12_LVL_CFG<<12)|(PIN_P0_13_LVL_CFG<<13) | \
                          (PIN_P0_14_LVL_CFG<<14)|(PIN_P0_15_LVL_CFG<<15)|(PIN_P0_16_LVL_CFG<<16)|(PIN_P0_17_LVL_CFG<<17)|(PIN_P0_18_LVL_CFG<<18)|(PIN_P0_19_LVL_CFG<<19)|(PIN_P0_20_LVL_CFG<<20) | \
                          (PIN_P0_21_LVL_CFG<<21)|(PIN_P0_22_LVL_CFG<<22)|(PIN_P0_23_LVL_CFG<<23)|(PIN_P0_24_LVL_CFG<<24)|(PIN_P0_25_LVL_CFG<<25)|(PIN_P0_26_LVL_CFG<<26)|(PIN_P0_27_LVL_CFG<<27) | \
                          (PIN_P0_28_LVL_CFG<<28))

#define PIN_PORT0_DIR_CFG (PIN_P0_0_DIR_CFG      |(PIN_P0_1_DIR_CFG<<1)  |(PIN_P0_2_DIR_CFG<<2)  |(PIN_P0_3_DIR_CFG<<3)  |(PIN_P0_4_DIR_CFG<<4)  |(PIN_P0_5_DIR_CFG<<5)  |(PIN_P0_6_DIR_CFG<<6)   | \
                          (PIN_P0_7_DIR_CFG<<7)  |(PIN_P0_8_DIR_CFG<<8)  |(PIN_P0_9_DIR_CFG<<9)  |(PIN_P0_10_DIR_CFG<<10)|(PIN_P0_11_DIR_CFG<<11)|(PIN_P0_12_DIR_CFG<<12)|(PIN_P0_13_DIR_CFG<<13) | \
                          (PIN_P0_14_DIR_CFG<<14)|(PIN_P0_15_DIR_CFG<<15)|(PIN_P0_16_DIR_CFG<<16)|(PIN_P0_17_DIR_CFG<<17)|(PIN_P0_18_DIR_CFG<<18)|(PIN_P0_19_DIR_CFG<<19)|(PIN_P0_20_DIR_CFG<<20) | \
                          (PIN_P0_21_DIR_CFG<<21)|(PIN_P0_22_DIR_CFG<<22)|(PIN_P0_23_DIR_CFG<<23)|(PIN_P0_24_DIR_CFG<<24)|(PIN_P0_25_DIR_CFG<<25)|(PIN_P0_26_DIR_CFG<<26)|(PIN_P0_27_DIR_CFG<<27) | \
                          (PIN_P0_28_DIR_CFG<<28))

/* Macros to access the configuration macros from pin_cnf.h */

#define PIN_IOCON_NOPULL       (0<<3)
#define PIN_IOCON_PULLDOWN     (1<<3)
#define PIN_IOCON_PULLUP       (2<<3)
#define PIN_IOCON_REPEATER     (3<<3)

#define PIN_IOCON_NO_HYS       (0<<5)
#define PIN_IOCON_HYS          (1<<5)

#define PIN_IOCON_NO_INV       (0<<6)
#define PIN_IOCON_INV          (1<<6)

#define PIN_IOCON_I2C_FAST     (0<<8)
#define PIN_IOCON_I2C_STANDARD (1<<8)
#define PIN_IOCON_I2C_FASTPLUS (2<<8)

#define PIN_IOCON_NO_OD        (0<<10)
#define PIN_IOCON_OD           (1<<10)

#define PIN_FILTMODE_BYPASS    (0<<11)
#define PIN_FILTMODE_1CLK      (1<<11)
#define PIN_FILTMODE_2CLK      (2<<11)
#define PIN_FILTMODE_3CLK      (3<<11)

#define PIN_IOCON_CLKDIV0      (0<<13)
#define PIN_IOCON_CLKDIV1      (1<<13)
#define PIN_IOCON_CLKDIV2      (2<<13)
#define PIN_IOCON_CLKDIV3      (3<<13)
#define PIN_IOCON_CLKDIV4      (4<<13)
#define PIN_IOCON_CLKDIV5      (5<<13)
#define PIN_IOCON_CLKDIV6      (6<<13)

/* PINASSIGN */
#define PIN_PA0_UART0_TXD(x)        ((x)&0xff)
#define PIN_PA0_UART0_RXD(x)        (((x)&0xff)<<8)
#define PIN_PA0_UART0_RTS(x)        (((x)&0xff)<<16)
#define PIN_PA0_UART0_CTS(x)        (((x)&0xff)<<24)

#define PIN_PA1_UART0_SCLK(x)       ((x)&0xff)
#define PIN_PA1_UART1_TXD(x)        (((x)&0xff)<<8)
#define PIN_PA1_UART1_RXD(x)        (((x)&0xff)<<16)
#define PIN_PA1_UART1_RTS(x)        (((x)&0xff)<<24)

#define PIN_PA2_UART1_CTS(x)        ((x)&0xff)
#define PIN_PA2_UART1_SCLK(x)       (((x)&0xff)<<8)
#define PIN_PA2_UART2_TXD(x)        (((x)&0xff)<<16)
#define PIN_PA2_UART2_RXD(x)        (((x)&0xff)<<24)

#define PIN_PA3_UART2_RTS(x)        ((x)&0xff)
#define PIN_PA3_UART2_CTS(x)        (((x)&0xff)<<8)
#define PIN_PA3_UART2_SCLK(x)       (((x)&0xff)<<16)
#define PIN_PA3_SPI0_SCK(x)         (((x)&0xff)<<24)

#define PIN_PA4_SPI0_MOSI(x)        ((x)&0xff)
#define PIN_PA4_SPI0_MISO(x)        (((x)&0xff)<<8)
#define PIN_PA4_SPI0_SSELSN_0(x)    (((x)&0xff)<<16)
#define PIN_PA4_SPI0_SSELSN_1(x)    (((x)&0xff)<<24)

#define PIN_PA5_SPI0_SSELSN_2(x)    ((x)&0xff)
#define PIN_PA5_SPI0_SSELSN_3(x)    (((x)&0xff)<<8)
#define PIN_PA5_SPI1_SCK(x)         (((x)&0xff)<<16)
#define PIN_PA5_SPI1_MOSI(x)        (((x)&0xff)<<24)

#define PIN_PA6_SPI1_MISO(x)        ((x)&0xff)
#define PIN_PA6_SPI1_SSELSN_0(x)    (((x)&0xff)<<8)
#define PIN_PA6_SPI1_SSELSN_1(x)    (((x)&0xff)<<16)
#define PIN_PA6_SCT_PIN0(x)         (((x)&0xff)<<24)

#define PIN_PA7_SCT_PIN1(x)         ((x)&0xff)
#define PIN_PA7_SCT_PIN2(x)         (((x)&0xff)<<8)
#define PIN_PA7_SCT_PIN3(x)         (((x)&0xff)<<16)
#define PIN_PA7_SCT_OUT0(x)         (((x)&0xff)<<24)

#define PIN_PA8_SCT_OUT1(x)         ((x)&0xff)
#define PIN_PA8_SCT_OUT2(x)         (((x)&0xff)<<8)
#define PIN_PA8_SCT_OUT3(x)         (((x)&0xff)<<16)
#define PIN_PA8_SCT_OUT4(x)         (((x)&0xff)<<24)

#define PIN_PA9_SCT_OUT5(x)         ((x)&0xff)
#define PIN_PA9_I2C1_SDA(x)         (((x)&0xff)<<8)
#define PIN_PA9_I2C1_SCL(x)         (((x)&0xff)<<16)
#define PIN_PA9_I2C2_SDA(x)         (((x)&0xff)<<24)

#define PIN_PA10_I2C2_SCL(x)        ((x)&0xff)
#define PIN_PA10_I2C3_SDA(x)        (((x)&0xff)<<8)
#define PIN_PA10_I2C3_SCL(x)        (((x)&0xff)<<16)
#define PIN_PA10_ADC_PIN_TRIG0(x)   (((x)&0xff)<<24)

#define PIN_PA11_ADC_PIN_TRIG1(x)   ((x)&0xff)
#define PIN_PA11_ACMP(x)            (((x)&0xff)<<8)
#define PIN_PA11_CLKOUT(x)          (((x)&0xff)<<16)
#define PIN_PA11_INT_BMAT(x)        (((x)&0xff)<<24)



/* Pin function Enable */
#define PIN_PE0_ACMP_I1(x)  ((x)<<0)
#define PIN_PE0_ACMP_I2(x)  ((x)<<1)
#define PIN_PE0_ACMP_I3(x)  ((x)<<2)
#define PIN_PE0_ACMP_I4(x)  ((x)<<3)
#define PIN_PE0_SWCLK(x)    ((x)<<4)
#define PIN_PE0_SWDIO(x)    ((x)<<5)
#define PIN_PE0_XTALIN(x)   ((x)<<6)
#define PIN_PE0_XTALOUT(x)  ((x)<<7)
#define PIN_PE0_RESETN(x)   ((x)<<8)
#define PIN_PE0_CLKIN(x)    ((x)<<9)
#define PIN_PE0_VDDCMP(x)   ((x)<<10)
#define PIN_PE0_I2C0_SDA(x) ((x)<<11)
#define PIN_PE0_I2C0_SCL(x) ((x)<<12)
#define PIN_PE0_ADC_0(x)    ((x)<<13)
#define PIN_PE0_ADC_1(x)    ((x)<<14)
#define PIN_PE0_ADC_2(x)    ((x)<<15)
#define PIN_PE0_ADC_3(x)    ((x)<<16)
#define PIN_PE0_ADC_4(x)    ((x)<<17)
#define PIN_PE0_ADC_5(x)    ((x)<<18)
#define PIN_PE0_ADC_6(x)    ((x)<<19)
#define PIN_PE0_ADC_7(x)    ((x)<<20)
#define PIN_PE0_ADC_8(x)    ((x)<<21)
#define PIN_PE0_ADC_9(x)    ((x)<<22)
#define PIN_PE0_ADC_10(x)   ((x)<<23)
#define PIN_PE0_ADC_11(x)   ((x)<<24)
#endif

