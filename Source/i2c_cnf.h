/** I2C library - Configuration
	Configuration of I2C peripheral
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef I2C_CNF_H
#define I2C_CNF_H


#define I2C_PHYS_DEVICE_NUM      4     ///! number of physical I2C devices

/* these values have to fit the according configuration structures */
#define I2C_VIRTUAL_DEVICE_NUM   1     ///! the number of actually used devices as configured in i2c_device_ptr
#define I2C_TIMING_PROFILES_NUM  2     ///! number of timing profile configurations
#define I2C_CHANNEL_CONFIG_NUM   1     ///! number of I2C configuration channels

#define I2C_QUEUE_SIZE          16     ///! number of entries in queue per device (power of two to optimize)


/** channel access macros */
#define I2C_CH_TMP102            0     ///! TMP102 temperature sensor
// dummies - don't use unless configures correctly
#define I2C_CH_EEPROM            2     ///! I2C EEPROM data transfer
#define I2C_CH_EEPROM_ACK        3     ///! I2C EEPROM ACK poll


#endif
