/** INT library - Configuration
	Configuration of interrupt levels
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef INT_CNF_H
#define INT_CNF_H

/* define all interrupt levels here (0 is the highest, 3 the lowest priority) */

#define SYSTICK_IRQ_PRIO        1 ///! Handler for time base creation - same prio as USB to avoid issues
#define WRAP_APP_IRQ_PRIO       3 ///! Wrapper interrupt used to wrap down to low priority
// unused
#define I2C_IRQ_PRIO            3
#define DMA_IRQ_PRIO            3
#define SPI_IRQ_PRIO            3
#define UART_IRQ_PRIO           3

#endif
