/** TMP102 mini driver
	Driver for TMP102 temperature sensor
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2016 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#include "global.h"
#include "sys.h"
#include "tmp102.h"
#include "i2c.h"
#include "prc.h"


#define TMP102_ACTIVE 0x80
#define TMP102_MASK   0x3F


volatile static u8 tmp102_state_read;
volatile static u8 tmp102_state_write;
static u8  tmp102_data_read[2];
static u8  tmp102_data_write[2];
static u16 tmp102_thigh;
static u16 tmp102_tlow;
static u16 tmp102_config;
static s16 tmp102_temperature;

/** Callback function for TMP102 I2C read transfer.
 */
static void TMP102_CallbackRead(void) {
	u16 temp;

	temp = ((u16)tmp102_data_read[0]<<8) | (u16)tmp102_data_read[1];
	switch (tmp102_state_read & TMP102_MASK) {
		case TMP102_REG_TEMP:
			tmp102_temperature = (s16)temp;
			break;
		case TMP102_REG_CFG:
			tmp102_config = temp;
			break;
		case TMP102_REG_THI:
			tmp102_thigh = temp;
			break;
		default:
			tmp102_tlow = temp;
	}
	tmp102_state_read = 0;
}

/** Callback function for TMP102 I2C write transfer.
 */
static void TMP102_CallbackWrite(void) {
	tmp102_state_write = 0;
}


/** Start write transfer
	@return I2C OK Status (see I2C_MST_TRANSFER_xxx)
 */
u8 TMP102_Write(u8 reg, u16 value) {
	//while (tmp102_state_write != 0) {};
	tmp102_state_write = reg | TMP102_ACTIVE;
	tmp102_data_write[0] = (u8)(value >> 8);
	tmp102_data_write[1] = (u8)value;
	return I2C_MasterTransfer(I2C_CH_TMP102, I2C_TYPE_MST_WRITE, reg, &tmp102_data_write[0], 2, TMP102_CallbackWrite);
}

/** Start temperature transfer
	@return I2C OK Status (see I2C_MST_TRANSFER_xxx)
 */
u8 TMP102_StartRead(u8 reg) {
	//while (tmp102_state_read != 0) {};
	tmp102_state_read = reg | TMP102_ACTIVE;
	return I2C_MasterTransfer(I2C_CH_TMP102, I2C_TYPE_MST_READ, reg, &tmp102_data_read[0], 2, TMP102_CallbackRead);
}

/** Read config.
	@return config
 */
u16 TMP102_GetConfig(void) {
	return tmp102_config;
}

/** Read temperature.
	@return temperature 256=1°C
 */
s16 TMP102_GetTemperature(void) {
	return tmp102_temperature;
}

/** Read temperature high.
	@return temperature 256=1°C
 */
s16 TMP102_GetTempHigh(void) {
	return tmp102_thigh;
}

/** Read temperature low.
	@return temperature 256=1°C
 */
s16 TMP102_GetTempLow(void) {
	return tmp102_thigh;
}

void TMP102_WaitForCompletion(void) {
	while(I2C_GetChannelPendingCount(I2C_CH_TMP102)>0);
}
