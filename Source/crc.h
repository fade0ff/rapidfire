#ifndef CRC_H
#define CRC_H

/** CRC library
	Cyclic Redundancy Check
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2017 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

/* CRC mode register MODE */

#define CRC_MODE_POLY_CCITT              0         ///! Use CRC-CCITT polynomial
#define CRC_MODE_POLY_CRC16              (1UL<<0)  ///! Use CRC16 polynomial
#define CRC_MODE_POLY_CRC32              (1UL<<1)  ///! Use CRC32 polynomial
#define CRC_MODE_WR_BITORDER_NO_REVERSE  0         ///! Bit order reverse for CRC_WR_DATA (per byte)
#define CRC_MODE_WR_BITORDER_REVERSE     (1UL<<2)  ///! No bit order reverse for CRC_WR_DATA (per byte)
#define CRC_MODE_WR_NO_COMPLEMENT        0         ///! Bit order reverse for CRC_WR_DATA (per byte)
#define CRC_MODE_WR_COMPLEMENT           (1UL<<3)  ///! No bit order reverse for CRC_WR_DATA (per byte)
#define CRC_MODE_SUM_BITORDER_NO_REVERSE 0         ///! Bit order reverse for CRC_SUM (per byte)
#define CRC_MODE_SUM_BITORDER_REVERSE    (1UL<<4)  ///! No bit order reverse for CRC_SUM (per byte)
#define CRC_MODE_SUM_NO_COMPLEMENT       0         ///! Bit order reverse for CRC_SUM (per byte)
#define CRC_MODE_SUM_COMPLEMENT          (1UL<<5)  ///! No bit order reverse for CRC_SUM (per byte)


/** Get CRC mode register.
	@return MODE register
*/
#define CRC_GetMode() (LPC_CRC->MODE)

/** Set CRC mode register.
	@param y value to write to MODE register
*/
#define CRC_SetMode(y)  {LPC_CRC->MODE = (u32)(y);}


/* CRC seed register SEED */

/** Get CRC seed register.
	@return SEED register
*/
#define CRC_GetSeed() (LPC_CRC->SEED)

/** Set CRC seed register.
	@param y value to write to SEED register
*/
#define CRC_SetSeed(y)  {LPC_CRC->SEED = (u32)(y);}


/* CRC sum register SUM (read only) */

/** Get CRC sum register.
	@return SUM register
*/
#define CRC_GetSum() (LPC_CRC->SUM)


/* CRC data register WR_DATA (write only) */

/** Get CRC data register.
	@return WR_DATA register
*/
#define CRC_GetData() (LPC_CRC->WR_DATA)

/** Set CRC data register.
	@param y value to write to WR_DATA register
*/
#define CRC_SetData8(y)  {*(u8*)&LPC_CRC->WR_DATA = (u8)(y);}   // perform 8bit writes
#define CRC_SetData16(y)  {*(16*)&LPC_CRC->WR_DATA = (u16)(y);} // perform 16bit writes
#define CRC_SetData32(y)  {LPC_CRC->WR_DATA = (u32)(y);}       // perform 32bit writes



#endif
