/** TMP102 mini driver
	Driver for TMP102 temperature sensor
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2016 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef TMP102_H
#define TMP102_H

#define TMP102_REG_TEMP 0 ///! Temperature Register (Read Only)
#define TMP102_REG_CFG  1 ///! Configuration Register (Read/Write)
#define TMP102_REG_TLO  2 ///! TLOW Register (Read/Write)
#define TMP102_REG_THI  3 ///! THIGH Register (Read/Write)


extern u8  TMP102_Write(u8 reg, u16 value);
extern u8  TMP102_StartRead(u8 reg);
extern s16 TMP102_GetTemperature(void);
extern s16 TMP102_GetTempHigh(void);
extern s16 TMP102_GetTempLow(void);
extern u16 TMP102_GetConfig(void);
extern void TMP102_WaitForCompletion(void);

#endif
