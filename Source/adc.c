/** ADC Library
	Definitions for calling IAP ROM functions
	LPC824 ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2017 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#include "global.h"
#include "sys.h"
#include "adc.h"

u32 adc_ctrl;

/** Initialize the ADC peripheral */
void adc_init(void) {
	SYSCON_PowerUp(SYSCON_POWERDOWN_ADC_PD);
	ADC_SetIntEnable(0);
	ADC_SetTrim(ADC_VRANGE);
	adc_ctrl = ((SYS_CLK_ADC_FRQ/ADC_SAMPLE_FREQ/25-1)&0xff) | ((ADC_LOW_POWER_MODE != 0)?ADC_CTRL_LPWRMODE:0);
	ADC_SetCtrl(adc_ctrl);
}

/** Shutdown ADC */
void adc_shutdown(void) {
	ADC_SetIntEnable(0);
	ADC_SetCtrl(0);
	SYSCON_PowerDown(SYSCON_POWERDOWN_ADC_PD);
}

/** Start ADC calibration */
void adc_start_calibration(void) {
	/* Set calibration mode */
	ADC_SetCtrl(ADC_CTRL_CALMODE | ADC_CTRL_CLKDIV_FREQ(500000));
}

/** Start sequence
    @param sequence index of sequence
    @param channels bitmask of channels to start
 */
void adc_start_conversion(u8 sequence, u32 channels) {
	u32 val = channels | ADC_SEQCTRL_SEQ_ENA;
	ADC_SetCtrl(adc_ctrl);
	ADC_SetSeqCtrl(sequence, val);
	ADC_SetSeqCtrl(sequence, val|ADC_SEQCTRL_START);
}
