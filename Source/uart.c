/** UART library
	Communication via asynchronous serial interface (UART)
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#include "global.h"
#include "sys.h"
#include "dma.h"     // DMA mode
#include "int_cnf.h" // IRQ mode
#include "uart.h"

/** Device RAM structure */
typedef struct {
#if UART_USE_RX_DMA
	dma_entry_t dma_link __attribute__ ((aligned(16))); ///! Link structure for ring buffer
#else
	u16  rx_idx;                 ///! Number of received bytes
#endif
#if !UART_USE_TX_DMA
	u16  tx_cnt;                 ///! Number of transmitted bytes
	u8  *tx_buf;                 ///! Pointer to transmit buffer
#endif
	u16 scanned;                 ///! Number of characters already scanned
	u16 xfercount_last;          ///! last xfercount
	u8  rx_buf[UART_RX_BUFSIZE]; ///! Receive buffer (16bit for invalid handling)
	u8  phys_ch;                 ///! Physical device number
} uart_ram_t;


/** Device RAM */
uart_ram_t uart_ch_ram[UART_NUM_DEVICES];

/** Lookup table to get virtual device number from physical UART device number */
u8 uart_reverse_lookup[UART_NUM_PHYS_DEVICES] = {0,1,2};

#if UART_USE_RX_DMA
const u8 uart_dma_rx_ch_lookup[UART_NUM_PHYS_DEVICES]   = { DMA_CH_USART0_RX, DMA_CH_USART1_RX, DMA_CH_USART2_RX };
const u8 uart_dma_rx_prio_lookup[UART_NUM_PHYS_DEVICES] = { DMA_PRIO_USART0_RX, DMA_PRIO_USART1_RX, DMA_PRIO_USART2_RX };
#endif
#if UART_USE_TX_DMA
const u8 uart_dma_tx_ch_lookup[UART_NUM_PHYS_DEVICES]   = { DMA_CH_USART0_TX, DMA_CH_USART1_TX, DMA_CH_USART2_TX };
const u8 uart_dma_tx_prio_lookup[UART_NUM_PHYS_DEVICES] = { DMA_PRIO_USART0_TX, DMA_PRIO_USART1_TX, DMA_PRIO_USART2_TX };
#endif

/** Get pointer to physical UART device.
	@param ch virtual UART device
	@return pointer to physical UART device
 */
LPC_UART_Type* UART_GetDevice(u8 ch) {
	return uart_config_ptr[ch].device;
}

/** Set new baudrate for virtual device.
	@param ch virtual UART device (channel)
	@param u32 baudrate in bits per second
	@return actual baud-rate in bits per second (can differ from the requested baud-rate)
 */
u32 UART_SetBaudRate( u8 ch, u32 baudrate) {
	LPC_UART_Type *device = uart_config_ptr[ch].device;

	/* U_PCLK = SYS_CLK_UART_FRQ/(1+(MULT/256))  [MULT=0.255]
	   baudrate = U_PCLK/(16*BRGVAL)
	            = SYS_CLK_UART_FRQ/((1+(MULT/256))*16*BRGVAL)
	            = SYS_CLK_UART_FRQ*16/((256 + MULT)*BRGVAL)
 	   ->
 	   BRGVAL   = SYS_CLK_UART_FRQ*16/baudrate/(256 + MULT)
	 */
	u32 fract = baudrate*(256+UART_FRGCTRL_MULT_VAL);
	u32 brgval = (SYS_CLK_UART_FRQ*16+fract/2)/fract; // round
	UART_SetDivider(device, brgval);
	return SYS_CLK_UART_FRQ*16/((256 + UART_FRGCTRL_MULT_VAL)*brgval);
}


/** Setup UART - needed before actual configuration.
	@param ch virtual device (channel)
*/
void UART_Setup(u8 ch) {
	u8  uart_ch;
	LPC_UART_Type *device = uart_config_ptr[ch].device;

	UART_SetFractDivider(UART_FRGCTRL_DIV(255));
	UART_SetFractMultiplier(UART_FRGCTRL_MULT(UART_FRGCTRL_MULT_VAL));

	switch ((u32)device) {
		case (u32)LPC_USART0:
			uart_ch = 0;
			break;
		case (u32)LPC_USART1:
			uart_ch = 1;
			break;
		default:
			uart_ch = 2;
	}
	uart_ch_ram[ch].phys_ch = uart_ch;
	uart_ch_ram[ch].scanned = 0;
	uart_ch_ram[ch].xfercount_last = UART_RX_BUFSIZE;
	uart_reverse_lookup[uart_ch] = ch;

#if UART_USE_RX_DMA
	u16 dma_rx_ch = uart_dma_rx_ch_lookup[uart_ch];
	dma_entry_t *link = &uart_ch_ram[ch].dma_link;
	// setup receive DMA
	DMA_EnableCh(dma_rx_ch,1);
	DMA_SetChConfig(dma_rx_ch, DMA_CFG_PERIPHREQEN|DMA_CFG_CHPRIORITY(uart_dma_rx_prio_lookup[uart_ch]));
	link->dst_addr = (u32)&uart_ch_ram[ch].rx_buf[0] + UART_RX_BUFSIZE - 1;
	DMA_SetChDestAdr(dma_rx_ch, link->dst_addr);
	link->src_addr = (u32)&UART_ReceiveRegister(device);
	DMA_SetChSrcAdr(dma_rx_ch, link->src_addr);
	link->next = link;
	DMA_SetChLinkAdr(dma_rx_ch, link); // ring buffer
	link->xfercfg = DMA_XFERCFG_WIDTH_8|DMA_XFERCFG_CFGVALID|DMA_XFERCFG_DSTINC_1|DMA_XFERCFG_RELOAD|DMA_XFERCFG_XFERCOUNT(UART_RX_BUFSIZE);
	DMA_SetChXferConfig(dma_rx_ch, link->xfercfg);
	DMA_SetTriggerCh(dma_rx_ch);
#else
	UART_SetIntEnable(device, UART_INTEN_RXRDY);
#endif
#if UART_USE_TX_DMA
	// enable transmit DMA
	u16 dma_tx_ch = uart_dma_tx_ch_lookup[uart_ch];
	DMA_EnableCh(dma_tx_ch,1);
	DMA_SetChConfig(dma_tx_ch, DMA_CFG_PERIPHREQEN|DMA_CFG_CHPRIORITY(uart_dma_tx_prio_lookup[uart_ch_ram[ch].phys_ch]));
	DMA_SetChDestAdr(dma_tx_ch,(u32)&UART_TransmitRegister(device));
	DMA_SetChLinkAdr(dma_tx_ch, 0);
#endif
#if !UART_USE_RX_DMA || !UART_USE_TX_DMA
	NVIC_SetPriority(UART0_IRQn+uart_ch, UART_IRQ_PRIO);
	NVIC_EnableIRQ(UART0_IRQn+uart_ch);
#endif
	// configure UART
	UART_SetConfig(device,uart_config_ptr[ch].line_ctrl);
}

/** Check if transmit is finished.
	@param ch virtual device (channel)
	@return 1 if finished, else 0
*/
u8 UART_IsTransmitFinished(u8 ch) {
#if UART_USE_TX_DMA
	return (DMA_IsActiveCh(uart_dma_tx_ch_lookup[uart_ch_ram[ch].phys_ch]) == 0);
#else
	return (uart_ch_ram[ch].tx_cnt == 0);
#endif
}

/** Transmit data.
	@param ch virtual device (channel)
	@param tx_buf pointer to transmit buffer
	@param tx_size number of bytes to transmit
	@return success 1: ok, 0: failed
*/
u8 UART_Transmit(u8 ch, u8* tx_buf, u16 tx_size) {
#if UART_USE_TX_DMA
	u16 dma_tx_ch = uart_dma_tx_ch_lookup[uart_ch_ram[ch].phys_ch];

	// check if Transmit is finished
	if (DMA_IsActiveCh(dma_tx_ch) != 0)
		return 0;

	// setup transmit DMA
	DMA_SetChSrcAdr(dma_tx_ch, (u32)tx_buf + tx_size -1);
	DMA_SetChXferConfig(dma_tx_ch, DMA_XFERCFG_WIDTH_8|DMA_XFERCFG_CFGVALID|DMA_XFERCFG_SRCINC_1|DMA_XFERCFG_SWTRIG|DMA_XFERCFG_XFERCOUNT(tx_size));
#else
	if (uart_ch_ram[ch].tx_cnt > 0) return 0;

	uart_ch_ram[ch].tx_buf = tx_buf+1;
	uart_ch_ram[ch].tx_cnt = tx_size-1;
	LPC_UART_Type *device = uart_config_ptr[ch].device;
	UART_SetData(device, *tx_buf);
	UART_SetIntEnable(device, UART_INTEN_RXRDY|UART_INTEN_TXRDY);
#endif
	return 1;
}


/** Get number of received bytes.
	@param ch virtual device (channel)
	@return number of received bytes
*/
u16 UART_GetReceiveCount(u8 ch) {
#if UART_USE_RX_DMA
	// xfercount is decremented - can't differentiate between 0 Bytes and UART_RX_BUFSIZE though
	return (u16)(uart_ch_ram[ch].xfercount_last - (DMA_GetChXferCount(uart_dma_rx_ch_lookup[uart_ch_ram[ch].phys_ch])))%UART_RX_BUFSIZE;
#else
	return (u16)(uart_ch_ram[ch].rx_idx + uart_ch_ram[ch].xfercount_last - UART_RX_BUFSIZE)%UART_RX_BUFSIZE;
#endif
}

/** Read data from the receive buffer.
	If a scan character (other than UART_NOSCAN) is given, only data until the last occurrence of this character is returned.
	This makes it easy to implement a string based protocols where each command ends with a certain end of line character.
	@param ch virtual device (channel)
	@param rx_buf pointer to copy received bytes to
	@param rx_size size of target buffer
	@param scan_char end of line character to scan for
	@return number of bytes copied to buffer (use UART_NOSCAN to disable this feature)
*/
u16 UART_Receive(u8 ch, u8* rx_buf, u16 rx_size, u16 scan_char) {
	u8 *buf = uart_ch_ram[ch].rx_buf;
	uart_ram_t *ram_ptr = &uart_ch_ram[ch];
	u16 s_idx = UART_RX_BUFSIZE - ram_ptr->xfercount_last;

	u16 d_idx;
	u16 len = UART_GetReceiveCount(ch);
	u16 scanned = ram_ptr->scanned;

	if (len > scanned) {
		/* new bytes received */
		if (scan_char != UART_NOSCAN) {
			/* scan for given EOL character and return all characters to this position
			   return nothing if no EOL character found
			   return rx_size bytes if > rx_size bytes received without EOL character */
			u16 c_idx = (s_idx + scanned) % UART_RX_BUFSIZE;
			u16 l = len;
			len = 0;
			for (; scanned < l; scanned++) {
				if (buf[c_idx] == (u8)scan_char) {
					len = ++scanned;
					break;
				}
				c_idx = (c_idx + 1) % UART_RX_BUFSIZE;
			}
		} else {
			/* return all bytes received */
			scanned = len;
		}
		ram_ptr->scanned = scanned-len;
		if (len > rx_size)
			len = rx_size;
		for (d_idx=0; d_idx < len; d_idx++) {
			*rx_buf++ = buf[s_idx];
			s_idx = (s_idx + 1) % UART_RX_BUFSIZE;
		}
		ram_ptr->xfercount_last = UART_RX_BUFSIZE - s_idx;
	} else
		len = 0;
	return len;
}

/* IRQ Handlers */
#if !UART_USE_RX_DMA || !UART_USE_TX_DMA

/** Global UART interrupt handler.
	@param ch virtual device (channel)
 */
void UART_IRQHandler(u8 ch) {
	LPC_UART_Type *device = uart_config_ptr[ch].device;
	uart_ram_t *ram_ptr = &uart_ch_ram[ch];

#if !UART_USE_RX_DMA
	/* receive */
	while ((UART_GetIntStatus(device) & UART_STAT_RXRDY) != 0) {
		ram_ptr->rx_buf[ram_ptr->rx_idx] = UART_GetData(device);
		ram_ptr->rx_idx = (ram_ptr->rx_idx+1) % UART_RX_BUFSIZE;
	}
#endif

#if !UART_USE_TX_DMA
	/* transmit */
	while ( ((UART_GetIntStatus(device) & UART_STAT_TXRDY) != 0) && (ram_ptr->tx_cnt > 0) ) {
		UART_SetData(device,*ram_ptr->tx_buf++)
		ram_ptr->tx_cnt--;
		if (ram_ptr->tx_cnt == 0)
			UART_ClrIntEnable(device, UART_STAT_TXRDY);
	}
#endif
	if (uart_config_ptr[ch].irq_callback != NULL) {
		(*uart_config_ptr[ch].irq_callback)();
	}
}

/** UART0 interrupt handler. Used to map to virtual device. */
void UART0_IRQHandler(void) {
	UART_IRQHandler(uart_reverse_lookup[0]);
}

/** UART1 interrupt handler. Used to map to virtual device. */
void UART1_IRQHandler(void) {
	UART_IRQHandler(uart_reverse_lookup[1]);
}

/** UART2 interrupt handler. Used to map to virtual device. */
void UART2_IRQHandler(void) {
	UART_IRQHandler(uart_reverse_lookup[2]);
}
#endif
