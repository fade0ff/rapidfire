A drop in replacement for the rapid fire PCB when converting an USB Competition Pro to a classic AMIGA/C64 Joystick.

See http://lemmini.de/RapidFire/RapidFire.html

---------------------------------------------------------------
Copyright 2020 Volker Oth - VolkerOth(at)gmx.de

Licensed under the Creative Commons Attribution 4.0 license
http://creativecommons.org/licenses/by/4.0/

Everything in this repository is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OF ANY KIND, either express or implied.